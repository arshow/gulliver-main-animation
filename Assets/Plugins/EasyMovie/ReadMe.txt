Easy Movie, developed by Hristo Ivanov (Kris Development) is a simple to use
tool for creating videos inside Unity from frames.

Here's how to set it up:
1. Create a folder named "Resources"
2. Create another folder that holds all the frames for your video and name it "<Your folder name here>".
3. Create a plane/quad/GUI Texture (legacy) and drag and drop the EasyMovieTexture.js script.

Settings:
1. "Load From Directory" is used to enter the path to your movie folder inside the Resource folder.
2. "Load Frames On Start" loads your frames from the directory on level start.
3. "Frames Per Second" is used to tell how many frames to go through each second.
4. "Video Speed Scale" speeds up or slows down the video and audio.
5. "Loop" repeats the video once the last frame is reached.
6. "Texture Name" refers to the name of the texture slot you wish to change inside your material.
7. When "GUI Texture" is checked the script tries to find a legacy GUITexture on the same object to use.
8. "Play Audio Clip" holds and audio clip to play on frame 0 (whenever the video is started).
9. "Audio Source" holds the AudioSource component to play the audio clip from. (Make sure it's AudioClip is empty).
10. "Load Level On Last Frame" loads the specified scene (by it's name) on the last frame. Leave empty if you don't want to load any scene.

The package includes example scenes.

Report any problems with the tool to: hropenmail01@abv.bg