﻿public var _loadFromDirectory : String = "";
public var _frames : Texture2D[];
public var loadFramesOnStart : boolean = false;
public var _framesPerSecond : int = 30;
public var _videoSpeedScale : float = 1;
public var _loop : boolean = false;
public var _textureName : String = "_MainTex";
public var _guiTexture : boolean = false;
private var t : float = 0;
private var lastFrameSwTime : float = 0;
var playAudioClip : AudioClip;
var audioSource : AudioSource;
public var _loadLevelOnLastFrame : String;

function Start () {
	if(loadFramesOnStart)
		StartCoroutine("LoadFrames");
}
function Update () {
	_videoSpeedScale = Mathf.Clamp(_videoSpeedScale,0,10);
	//return;
	if(audioSource){
		if(t == 0)
			audioSource.PlayOneShot(playAudioClip);
	
		audioSource.pitch = _videoSpeedScale;
	}
	
	var ti : int = parseInt(t);
			
		if(!_guiTexture)
			if(GetComponent(Renderer))
				GetComponent(Renderer).material.SetTexture(_textureName,_frames[ti]);
			else
				Debug.Log("No Renderer was found on object " + gameObject.name + "!");
	
		else
			if(GetComponent(GUITexture))
				GetComponent(GUITexture).texture = _frames[ti];
			else
				Debug.Log("No (Legacy) GUI Texture component was found on object " + gameObject.name + "!");
	
	if(t < _frames.Length-1)
		t += Time.deltaTime * _framesPerSecond * _videoSpeedScale;
	if(_loop)
		if(t >= _frames.Length -1){
			t = 0;
		}
	
	if(_loadLevelOnLastFrame != "")
		if(t >= _frames.Length -1)
			Application.LoadLevel(_loadLevelOnLastFrame);
}
function LoadFrames () {
	if(_loadFromDirectory != ""){
		var _fram : Object[] = Resources.LoadAll(_loadFromDirectory, typeof(Texture2D));
		_frames = new Texture2D[_fram.length];
		for(var f = 0; f < _fram.length; f++){
			_frames[f] = _fram[f];
		}
	}
}