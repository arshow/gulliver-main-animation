﻿@CustomEditor (EasyMovieTexture)
#if (UNITY_EDITOR)
public class MovieTextureEditor extends Editor
{
	function OnInspectorGUI()
    {
		var myScript : EasyMovieTexture = target;
        DrawDefaultInspector();
		
		//GUILayout.Label("Load frames from:");
		//GUILayout.BeginHorizontal();
		//GUILayout.Label("../Resources/",GUILayout.Width(80));
		//myScript._loadFromDirectory = GUILayout.TextField(myScript._loadFromDirectory);
		//GUILayout.EndHorizontal();
		if(GUILayout.Button("Load Frames From Folder Path"))
			myScript.LoadFrames();
	}
}
#endif