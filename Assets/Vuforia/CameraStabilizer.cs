﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class CameraStabilizer : MonoBehaviour
{
    public static CameraStabilizer _;

    VuforiaARController vuforia;


    Vector3 PrevPosition;
    Vector3 PrevRotation;

    Vector3 FixedPosition;
    Vector3 FixedRotation;

    Vector3 VuforiaPosition; 
    Vector3 VuforiaRotation;


    Vector3 tmpV3;

    [Range(5f, 50f)]
    public float SmoothSpeed = 15f;
    [Range(0.001f, 0.1f)]
    public float PositionTreshold = 0.005f;
    [Range(0.1f, 10f)]
    public float RotationTreshold = 0.1f;

    private void Awake(){
        _ = this;
    }

    void Start()
    {
        vuforia = VuforiaARController.Instance;
        vuforia.RegisterTrackablesUpdatedCallback(OnTrackablesUpdated);
        vuforia.RegisterVuforiaStartedCallback(OnInitialized);
    }

    public void OnTrackablesUpdated()
    {
        if (! this.isActiveAndEnabled ) return;

        VuforiaPosition = transform.position;
        VuforiaRotation = transform.rotation.eulerAngles;

        transform.rotation = Quaternion.Euler(PrevRotation);
        transform.position = PrevPosition;


        if (Mathf.Abs(VuforiaPosition.x - PrevPosition.x) > PositionTreshold) FixedPosition.x = VuforiaPosition.x;
		if (Mathf.Abs(VuforiaPosition.y - PrevPosition.y) > PositionTreshold) FixedPosition.y = VuforiaPosition.y;
		if (Mathf.Abs(VuforiaPosition.z - PrevPosition.z) > PositionTreshold) FixedPosition.z = VuforiaPosition.z;

		if (Mathf.Abs(VuforiaRotation.x - PrevRotation.x) > RotationTreshold) FixedRotation.x = VuforiaRotation.x;
		if (Mathf.Abs(VuforiaRotation.y - PrevRotation.y) > RotationTreshold) FixedRotation.y = VuforiaRotation.y;
		if (Mathf.Abs(VuforiaRotation.z - PrevRotation.z) > RotationTreshold) FixedRotation.z = VuforiaRotation.z;

    }

    public void OnInitialized()
    {
        FixedPosition = PrevPosition = transform.position;
        FixedRotation = PrevRotation = transform.rotation.eulerAngles;
    }

    void LateUpdate()
    {

        transform.position = Vector3.Lerp(transform.position, FixedPosition, Time.deltaTime * SmoothSpeed);
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(FixedRotation), Time.deltaTime * SmoothSpeed);
       
        PrevPosition = transform.position;
        PrevRotation = transform.rotation.eulerAngles;

    }


   
}
