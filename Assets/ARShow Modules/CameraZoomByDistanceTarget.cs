﻿using System;
using System.Collections;
using System.Collections.Generic;
using NatCamU.Core.Platforms;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class CameraZoomByDistanceTarget : MonoBehaviour {

    public static CameraZoomByDistanceTarget _;
    float TargetDistance = 0f;

	Text DistanceTXT;
    Text zoomTXT;
	Transform ArCamera;
	Transform TargetImage;
	float distanceFromStage = 0;
    bool targetVisible = false;
	bool mouseDown = false;
	float startTapTime;
	float maxIOSZoom = 1f;
	float maxAndroidZoom = 100f;
    bool IsZoomEnabled = false;

    #if UNITY_IOS
    float currentZoom = 1;
	NatCamDeviceiOS iosCam;
    #else
	float currentZoom = 0;
    #endif

	void Awake () {
		_ = this;
		if (ArCamera == null)
		{
			try
			{
				ArCamera = GameObject.Find("ARCamera").transform;
			}
			catch (Exception e)
			{
				Debug.LogError("### Zoom Handler Error | Can't find ARCamera Object!");
			}
		}

		if (TargetImage == null)
		{
			try
			{
				TargetImage = GameObject.Find("Static Target").transform;
			}
			catch (Exception e)
			{
				Debug.LogError("### Zoom Handler Error | Can't find 'Static Target' Object!");
			}
		}

		if (DistanceTXT == null)
		{
			try
			{
				DistanceTXT = GameObject.Find("Distance TXT").GetComponent<Text>();
			}
			catch (Exception e)
			{
				Debug.LogWarning("### Zoom Handler Warning | Can't find 'Distance TXT' Object!");
			}
		}

		if (zoomTXT == null)
		{
			try
			{
				zoomTXT = GameObject.Find("Zoom TXT").GetComponent<Text>();
			}
			catch (Exception e)
			{
				Debug.LogWarning("### Zoom Handler Warning | Can't find 'Distance TXT' Object!");
			}
		}

		TrackableStatusDetector.OnTargetDetectionChange += OnTargetDetection;
	}

    private void Start()
    {
        #if UNITY_IOS
        iosCam = new NatCamDeviceiOS();
        maxIOSZoom = iosCam.MaxZoomRatio(0);
        #elif UNITY_ANDROID

        //string _maxAndroidZoom = "";
        //CameraDevice.Instance.GetField("max-zoom", out _maxAndroidZoom);
        //if (_maxAndroidZoom != null) float.TryParse(_maxAndroidZoom, out maxAndroidZoom);


        #endif
    }

    private void FixedUpdate()
    {
        if (ArCamera == null) return;
		distanceFromStage = Vector3.Distance(ArCamera.position, TargetImage.position);
		if (DistanceTXT != null) DistanceTXT.text = "Distance: " + distanceFromStage.ToString("F2");

        if (TargetDistance <= 0 || !targetVisible || !IsZoomEnabled) return;

        if (distanceFromStage > TargetDistance)
        {
            #if UNITY_IOS
            currentZoom += 0.02f;
            #else
            currentZoom += 0.25f;
            #endif
            SetZoom(currentZoom);
        }

    }

    public void SetZoomDistance(float distance){
        if (distance == 0) IsZoomEnabled = false;
        else IsZoomEnabled = true;

        TargetDistance = distance;
        SetZoom(0f);
        currentZoom = 0f;
    }

	

    void SetZoom(float zoom)
	{
		// (int)Mathf.Ceil(currentZoom)


        #if UNITY_IOS && !UNITY_EDITOR
        if (zoom < 1f) zoom = 1f;
        else if (zoom > maxIOSZoom) zoom = maxIOSZoom;
        iosCam.SetZoom(0, zoom);
        #else
		if (zoom < 0f) zoom = 0f;
        if (zoom > maxAndroidZoom) zoom = maxAndroidZoom;
		Vuforia.CameraDevice.Instance.SetField("zoom", Mathf.Ceil(zoom).ToString());
        #endif

		if (zoomTXT != null) zoomTXT.text = "Zoom: " + zoom.ToString("F2");
	}

	



	/**
     *  Handle target detection
     */
	void OnTargetDetection(bool visible)
	{
        targetVisible = visible;
	}
}
