﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utils{

	public static float Map(float value, float fromSource, float toSource, float fromTarget, float toTarget)
	{
		return (value - fromSource) / (toSource - fromSource) * (toTarget - fromTarget) + fromTarget;
	}
}
