﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMask : MonoBehaviour
{
    public static FollowMask _;
    public GameObject TouchPlane;
    public Camera cam;
    public Transform mask;
    public GameObject maskMarker;

    private RaycastHit hit;
    private Ray ray;
    private bool initialized = true;
    private bool mousedown = false;
    private float MaskInitialY;
    private Vector3 tmpV3;

    void Awake()
    {
        _ = this;
        if (TouchPlane == null) TouchPlane = gameObject;
        if (cam == null) cam = Camera.main;

        // Check if all OK
        if (cam == null)
        {
            Debug.LogError("FollowMask Error: Camera not found!");
            initialized = false;
        }

        if (mask == null)
        {
            Debug.LogError("FollowMask Error: Mask object not set!");
            initialized = false;
        }

        if (initialized)
        {
            MaskInitialY = mask.position.y;
        }

        if (!ARShowSettings._.IsMonitor) maskMarker.SetActive(false);
    }

    void Update()
    {
        if (!initialized) return;

        #if UNITY_EDITOR
            HandleMouse();
        #else
            HandleTouch();
        #endif
	}


    void HandleTouch(){
		if (Input.touchCount == 1){
			if (Input.GetTouch(0).phase == TouchPhase.Began || Input.GetTouch(0).phase == TouchPhase.Moved){
                DoRaycast(Input.GetTouch(0).position);
			}
		}
    }

    void HandleMouse(){
        if (Input.GetMouseButtonDown(0)) mousedown = true;
        if (Input.GetMouseButtonUp(0)) mousedown = false;

        if( mousedown ){
            DoRaycast(Input.mousePosition);
        }
    }


    void DoRaycast( Vector3 point ){

        ray = cam.ScreenPointToRay(point);
		if (Physics.Raycast(ray, out hit))
		{
            if (hit.collider.gameObject.name == "Mask Draging Area") MoveMask( hit.point);
		}

	}

    void MoveMask(Vector3 pos){
        tmpV3 = pos;
        tmpV3.y = MaskInitialY;
        mask.position = tmpV3;

        if(ARShowSettings._.IsMonitor) ARShowCommunicationModule._.SendMaskPosition(mask.position);
    }

    // ------------------------

    public void SetMaskPosition(Vector3 pos){
		tmpV3 = pos;
		tmpV3.y = MaskInitialY;
		mask.position = tmpV3;
    }
}
