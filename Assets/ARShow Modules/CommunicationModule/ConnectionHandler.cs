﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;
using System.IO;
using System;

public class ConnectionHandler : MonoBehaviour
{

    public static ConnectionHandler _;
    public static Action<string> OnConnected;
    public ARShowCommunicationModule CommunicationModule;

    TcpClient client;
    NetworkStream stream;
    StreamWriter theWriter;
    StreamReader theReader;
    string recievedData = "";
    string lastMessage = "";
    bool ReadyToNextMessage = false;
    string ip;
    bool waitingForPingResponse = false;
    DateTime NextConnectionCheck;
    DateTime PingSentTime;
    bool IsConnected = false;
    WaitForEndOfFrame weof;

    void Awake()
    {
        _ = this;
    }


    public enum DebugActions { SET_CONNECTED, SET_DISCONNECTED, SET_CONNECTING, RECIEVED_COMMAND }
    public void SendDebug(DebugActions act, string param = "")
    {
        if (DebugHandler._ == null) return;

        switch (act)
        {
            case DebugActions.SET_CONNECTED:
                {
                    DebugHandler._.OnServerConnected(param);
                    break;
                }
            case DebugActions.SET_DISCONNECTED:
                {
                    DebugHandler._.OnServerDisconnected();
                    break;
                }
            case DebugActions.SET_CONNECTING:
                {
                    DebugHandler._.OnConnectingToServer();
                    break;
                }
            case DebugActions.RECIEVED_COMMAND:
                {
                    DebugHandler._.OnCammandRecieved(param);
                    break;
                }
        }
    }

    void Start()
    {
        //Connect("127.0.0.1");
        if (!String.IsNullOrEmpty(ARShowSettings._.AutoConnectIP))
        {
            Connectmanualy(ARShowSettings._.AutoConnectIP);
        }
        weof = new WaitForEndOfFrame();
        //SendDebug(DebugActions.SET_DISCONNECTED);

    }

    public void Connectmanualy(string ip)
    {
        this.ip = ip;
        Connect(ip);
    }

    void OnApplicationQuit()
    {
        CloseConnection();
    }

    void Update()
    {

        if (NextConnectionCheck < DateTime.Now && IsConnected)
        {
            PingConnection();
        }

        if (waitingForPingResponse && (DateTime.Now - PingSentTime).TotalSeconds > 3f && IsConnected)
        { // Connection Lost
            // Handle connection lost
            Debug.Log("Connection Lost!");
            IsConnected = false;
            CloseConnection();
            Connect(ip);
        }

        // --


        if (!ReadyToNextMessage || !IsConnected) return;

        if (stream.DataAvailable)
        {
            ReadyToNextMessage = false;
            ReadMessage();
        }


    }

    IEnumerator PingIPAddress(string ip, Action<bool> callback = null)
    {
        Debug.Log("Pinging " + ip + " ... ");
        yield return weof;


        Ping p = new Ping(ip);
        while (!p.isDone)
        {
            yield return weof;
        }

        if (p.time >= 0)
        {  // Ping OK
            Debug.Log("Ping OK");
            if (callback != null) callback(true);
        }
        else
        {
            Debug.Log("Ping Error!");
            if (callback != null) callback(false);
        }

    }

	public void Connect(string ip){
        SendDebug(DebugActions.SET_CONNECTING, ip);

        StartCoroutine( PingIPAddress( ip, (bool ok)=>{
            if( ok ){

				try
				{
					client = new TcpClient(ip, 3001);
					stream = client.GetStream();
					theWriter = new StreamWriter(stream);
					theReader = new StreamReader(stream);

					ReadyToNextMessage = true;
					IsConnected = true;
					SendDebug(DebugActions.SET_CONNECTED, ip);
                    if (OnConnected != null) OnConnected(ip);
                    // Request zoom settings
                    ConnectionHandler._.SendMessageToServer("zoom");
					SetNextPingTime();

				}
				catch (Exception e)
				{
					SendDebug(DebugActions.SET_DISCONNECTED);
				}

            }else{
                Debug.Log("Connection Failed!!!");
                SendDebug(DebugActions.SET_DISCONNECTED);
            }
        } ) );

		

	}

    public void ConnectFromIPList(int index){
        if (index == 0) return;

        switch( index ){
            case 1 : {
                Connectmanualy("192.168.1.2");
                break;
            }
			case 2 : {
				Connectmanualy("10.0.1.2");
				break;
			}
            default:{
                break;
            }
        }
    }

	void CloseConnection(){
		if (!IsConnected) return;
        IsConnected = false;
		theWriter.Close();
		theReader.Close();
		stream.Close();
		client.Close();
	}


    public void PingConnection(){
        SetNextPingTime();
        PingSentTime = DateTime.Now;
        waitingForPingResponse = true;

        theWriter.WriteLine("ping");
        theWriter.Flush();

        Debug.Log("Sending Ping...");
    }

    void SetNextPingTime(){
        NextConnectionCheck = DateTime.Now.AddSeconds(UnityEngine.Random.Range(30, 90));
    }

	public void SendMessageToServer(string msg)
	{
        if (!IsConnected) return;
        Debug.Log("Sending Message: " + msg);
		SetNextPingTime();
        theWriter.WriteLine(msg);
		theWriter.Flush();
	}

	void ReadMessage(){
		Loom.RunAsync (() => {

			recievedData = theReader.ReadLine();
			stream.Flush();

            Debug.Log("Data Recieve! | " + recievedData);

            if (recievedData == "pong")
            {
                waitingForPingResponse = false;
                ReadyToNextMessage = true;
                Debug.Log("Connection OK!");
                return;
            }



			Loom.QueueOnMainThread(()=>{
                SetNextPingTime();
				lastMessage = recievedData;
				CommunicationModule.HandleCommandString(recievedData);
				ReadyToNextMessage = true;
			});

		});
	}	
}
