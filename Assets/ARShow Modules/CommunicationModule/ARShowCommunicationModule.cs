﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Vuforia;

public class ARShowCommunicationModule : MonoBehaviour {
	public static ARShowCommunicationModule _;

    public enum KeyName { UP, DOWN, LEFT, RIGHT, FORWARD, BACKWARD, SPACE };
    public static Action<KeyName> OnKeyDown;
	public static Action<KeyName> OnKeyUp;
	// -----------------
    public MainInterpreter interpreter;
    public ARShowUDPHandler UDPHandler;
    public GameObject cameraOverlay;
    GameObject arCamera;
	string[] commandsArray;
	string[] command;
	int commScene, commLayer, commAction, commFunc;
	string tmpString;
    string cmdString;
    char[] dotsSeparator;
    public CamraStabilizer2 cameraStabelizer;
    float ZoomTargetDistance = 0f;
    string lastKey = "";
    int lastKeyStatus = -1;
    WaitForEndOfFrame weof;
    KeyName kName;
	void Awake(){
		_ = this;
        dotsSeparator = new char[] { ':' };
	}

    private void Start()
    {

        weof = new WaitForEndOfFrame();
		if (arCamera == null)
		{
			try
			{
				arCamera = GameObject.Find("ARCamera");

			}
			catch (Exception e)
			{
				Debug.LogError("### Communication Module Error | Can't find ARCamera Object!");
			}
		}

        if (cameraStabelizer == null && arCamera != null)
		{
			try
			{
				cameraStabelizer = GameObject.Find("ARCamera").GetComponent<CamraStabilizer2>();
				
			}
			catch (Exception e)
			{
				Debug.LogError("### Communication Module Error | Can't find ARCamera / CamraStabilizer2 Object!");
			}
		}

		if (interpreter == null)
		{
			try
			{
                interpreter = GameObject.Find("Interpreter").GetComponent<MainInterpreter>();
			}
			catch (Exception e)
			{
				Debug.LogError("### Communication Module Error | Can't find Interpreter Object!");
			}
		}

        if(cameraOverlay == null){
			try
			{
				cameraOverlay = GameObject.Find("CameraOverlay");
                if(cameraOverlay != null) cameraOverlay.SetActive(false);
			}
			catch (Exception e)
			{
				Debug.LogError("### Communication Module Error | Can't find 'CameraOverlay' Object!");
			}
        }
    }

    public void SendSeatAndRow(string row, string seat){
        cmdString = "setseat,main," + row + "," + seat;
        ConnectionHandler._.SendMessageToServer(cmdString);
        if (TrackableStatusDetector.IsTracking) SendTrackingStatus(true);
    }

    public void SendTrackingStatus(bool tracking){
        ConnectionHandler._.SendMessageToServer("settracking," + ((tracking) ? "true":"false") );
	}

    public void HandleCommandString(string comm = "")
    {
		if (comm == null || comm == "") return;

		commandsArray = comm.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

        // On Stabilization command
        if(commandsArray[0] == "stb"){
            HandleStabilizationSettingCommand();
            return;
        }

        // On World Offset command
        if( commandsArray[0] == "ofs" ){
            HandleWorldOffsetSettingCommand();
            return;
        }

        // On Reset App command
        if( commandsArray[0] == "rst" ){
            HandleResetCommand();

            return;
        }

		// On Pause command
		if (commandsArray[0] == "pause")
		{
            Time.timeScale = 0;

			return;
		}

		// On Pause command
		if (commandsArray[0] == "unpause")
		{
			Time.timeScale = 1;

			return;
		}

		// On Reset layer command
		if (commandsArray[0] == "stoplayer")
		{
            if (!int.TryParse(commandsArray[1], out commLayer)) return;
            Debug.Log("STOP LAYER COMMAND | Layer: " + commLayer);
            interpreter.ResetLayer(commLayer);
			return;
		}

        // On Zoom settings command
        // example command: [zoom,15,40,100]

		if (commandsArray[0] == "zoom")
		{
            
			if (!float.TryParse(commandsArray[1], out ZoomTargetDistance)) return;
            CameraZoomByDistanceTarget._.SetZoomDistance(ZoomTargetDistance);
			return;
		}

		// Catch keyboard events
		if (commandsArray[0] == "key")
		{
            if (commandsArray[1] == null || commandsArray[1].Trim() == "") return;
            else lastKey = commandsArray[1];
            if (!int.TryParse(commandsArray[2], out lastKeyStatus)) return;

            HandleKeyboardEvents(lastKey, (lastKeyStatus == 1));

			return;
		}

		// Handle animation commands
		if (commandsArray[0] == "anm")
		{
            if (commandsArray.Length < 4) return;
			if (commandsArray[1] == null || commandsArray[1].Trim() == "") return;
            if (commandsArray[2] == null || commandsArray[2].Trim() == "") return;
            if (commandsArray[3] == null || commandsArray[3].Trim() == "") return;

            HandleAnimationCommand(commandsArray[1], commandsArray[2], commandsArray[3]);
			return;
		}

		// Handle switch scene commands
		if (commandsArray[0] == "scene")
		{
			if (commandsArray.Length < 2) return;
			if (commandsArray[1] == null || commandsArray[1].Trim() == "") return;

			HandleSwitchSceneCommand(commandsArray[1]);
			return;
		}

		// Handle special actions commands
		if (commandsArray[0] == "spa")
		{
			if (commandsArray.Length < 3) return;
			if (commandsArray[1] == null || commandsArray[1].Trim() == "") return;
            if (commandsArray[2] == null || commandsArray[2].Trim() == "") return;

			HandleSpecialActionCommand(commandsArray[1], commandsArray[2]);
			return;
		}


		// On standby command
		if (commandsArray[0] == "standby")
		{
            StartCoroutine(enterStandByMode());
			return;
		}

		// On wakeup command
		if (commandsArray[0] == "wakeup")
		{

            cameraOverlay.SetActive(false);
            arCamera.SetActive(true);
			Time.timeScale = 1;
            ResumeVuforia();
			return;
		}


		// ----

		ConnectionHandler._.SendDebug(ConnectionHandler.DebugActions.RECIEVED_COMMAND, comm);
    }

    IEnumerator enterStandByMode(){

        StopVuforia();
		cameraOverlay.SetActive(true);
        yield return weof;
        arCamera.SetActive(false);
		Time.timeScale = 0;
	}

    void StopVuforia(){
		CameraDevice.Instance.Stop();
    }

    void ResumeVuforia(){
        CameraDevice.Instance.Start();
    }


    /**
     *  Handle keyboard events
     */ 
    public void HandleKeyboardEvents(string key, bool isDown){

        if (key == "fwd" ) kName = KeyName.FORWARD;
        else if (key == "back") kName = KeyName.BACKWARD;
        else if (key == "left") kName = KeyName.LEFT;
        else if (key == "right") kName = KeyName.RIGHT;
        else if (key == "up") kName = KeyName.UP;
        else if (key == "down") kName = KeyName.DOWN;
        else if (key == "space") kName = KeyName.SPACE;

        if(isDown){
            if (OnKeyDown != null) OnKeyDown(kName);
        }else{
            if (OnKeyUp != null) OnKeyUp(kName); 
        }

    }

    /**
     *  Handle regular animation command
     */
    public void HandleAnimationCommand(string layer, string animation_index, string action_type){
        Debug.Log("ANIMATION COMMAND |  Layer: " + layer + " Index: " + animation_index + " Type: " + action_type);

		// Parse command
		if (!int.TryParse(layer, out commLayer)) return;
		if (!int.TryParse(animation_index, out commAction)) return;
		if (!int.TryParse(action_type, out commFunc)) return;

        interpreter.ExecuteCommand(commLayer, commAction, commFunc);  

    }


    /**
     *  Handle specal action command
     */
    public void HandleSpecialActionCommand(string action_index, string action_type){
        Debug.Log("SPECIAL ACTION COMMAND | Index: " + action_index + " Type: " + action_type);

        if (!int.TryParse(action_index, out commAction)) return;
        if (!int.TryParse(action_type, out commFunc)) return;

        interpreter.ExecuteSpecialAction(commAction, commFunc);
    }

    /**
     *  Handle switch scene command
     */
    public void HandleSwitchSceneCommand(string scene_index){
        Debug.Log("SWITCH SCENE COMMAND | Index: " + scene_index);

        if (!int.TryParse(scene_index, out commScene)) return;

        interpreter.SwitchScene(commScene);
    }


    public void HandleStabilizationSettingCommand(){
        //Debug.Log("Stabilization command: " + commandsArray[1] + " | " + commandsArray[2] + " | " +commandsArray[3]);

        cameraStabelizer.enabled = (commandsArray[1] == "true");
        cameraStabelizer.PositionTreshold = float.Parse(commandsArray[2]);
        cameraStabelizer.RotationTreshold = float.Parse(commandsArray[3]);
    }

    public void HandleWorldOffsetSettingCommand(){
        Debug.Log("Offset command: " + commandsArray[1] + " | " + commandsArray[2] + " | " + commandsArray[3]);

        WorldHandler._.SetOffset( float.Parse( commandsArray[1] ) , float.Parse(commandsArray[2]) , float.Parse(commandsArray[3]));
    }

    public void HandleResetCommand(){
        Debug.Log("RESET SHOW COMMAND");
        interpreter.ResetShow();
    }



    string[] udpData;
    float x, y, z;
    Vector3 mPos = Vector3.zero;
    public void HandleUDPData(string data){
        udpData = data.Split(dotsSeparator, StringSplitOptions.RemoveEmptyEntries);

        switch( udpData[0] ){
            case "mpos" : {
                    if (!float.TryParse(udpData[1], out x)) return;
				if (!float.TryParse(udpData[2], out y)) return;
				if (!float.TryParse(udpData[3], out z)) return;
                    mPos.x = x;
                    mPos.y = y;
                    mPos.z = z;
                    FollowMask._.SetMaskPosition(mPos);
                break;
            }
        }

    }

    public void SendMaskPosition(Vector3 pos){
        if (UDPHandler != null) UDPHandler.SendUDPMessage("mpos:" + pos.x + ":" + pos.y + ":" + pos.z);
    }

}
