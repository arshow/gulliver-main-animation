﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

public class ARShowUDPHandler : MonoBehaviour {

    private string ip;
    UdpClient udpClient;
    private bool connected = false;
    private bool IsSender = false;
    private bool stopListening = false;
    private Byte[] receiveBytes;
    private string recievedString;
    private IPEndPoint RemoteIpEndPoint;
    private void Awake()
    {
        ConnectionHandler.OnConnected += Init;
    }

	private void OnApplicationQuit()
	{
		try
		{
			// shutdown 'the friendly' way
			stopListening = true;
			
			if (udpClient != null)
			{
				udpClient.Close();
				udpClient = null;
			}

		}
		catch
		{
			
		}
	}


	void Init (string ip) {
        this.ip = ip;

        if (ARShowSettings._.IsMonitor) InitAsSender();
        else InitAsReciever();
	}

    void InitAsSender(){
        
		try
		{
			udpClient = new UdpClient();
			udpClient.Connect(this.ip, ARShowSettings._.UDPOutgoungPort);
			connected = true;
            IsSender = true;
			Debug.Log("UDP Client Established");
		}
		catch (Exception e)
		{
			Debug.LogError("UDPHandler error: " + e.Message);
		}
    }

    void InitAsReciever(){
        try{
            udpClient = new UdpClient(ARShowSettings._.UDPIncommingPort);
			connected = true;
            IsSender = false;
			
            stopListening = false;

            Loom.RunAsync(()=>{
                Debug.Log("UDP Server Established, listening...");
                RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, ARShowSettings._.UDPIncommingPort);

                while( !stopListening ){
					receiveBytes = udpClient.Receive(ref RemoteIpEndPoint);
                    recievedString = Encoding.ASCII.GetString(receiveBytes);
                    Loom.QueueOnMainThread(()=>{
                        if( ARShowCommunicationModule._ != null ) ARShowCommunicationModule._.HandleUDPData(recievedString);
                    });
                }
            });
		}
		catch (Exception e)
		{
			Debug.LogError("UDPHandler error: " + e.Message);
		}
    }
	
    public void SendUDPMessage(string message){
		if (!connected || !IsSender) return;

        udpClient.Send(Encoding.ASCII.GetBytes(message), message.Length);
    }
   
}
