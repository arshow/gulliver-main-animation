﻿using System;
using System.Collections;
using System.Collections.Generic;
using NatCamU.Core.Platforms;
using UnityEngine;
using UnityEngine.UI;

public class CameraZoomHandler : MonoBehaviour
{
    public static CameraZoomHandler _;
    Text zoomTXT;
    bool mouseDown = false;
    float startTapTime;
    NatCamDeviceiOS iosCam;
    float maxIOSZoom = 1f;

    Text DistanceTXT;
    Transform ArCamera;
    Transform TargetImage;
    float distanceFromStage = 0;

    // Zoom Settings
    float minDistanceANDROID = 0f;
    float maxDistanceANDROID = 0f;
    float maxZoomANDROID = 0f;
    float minDistanceIOS = 0f;
    float maxDistanceIOS = 0f;
    float maxZoomIOS = 0f;

    bool TargetVisible = false;
    bool WaitingForAutoZoom = false;

    WaitForEndOfFrame weof;


#if UNITY_IOS
    float currentZoom = 1;
#else
     float currentZoom = 0;
#endif   

    private void Awake()
    {
        _ = this;
        weof = new WaitForEndOfFrame();

        if (ArCamera == null)
        {
            try
            {
                ArCamera = GameObject.Find("ARCamera").transform;
            }
            catch (Exception e)
            {
                Debug.LogError("### Zoom Handler Error | Can't find ARCamera Object!");
            }
        }

        if (TargetImage == null)
        {
            try
            {
                TargetImage = GameObject.Find("Static Target").transform;
            }
            catch (Exception e)
            {
                Debug.LogError("### Zoom Handler Error | Can't find 'Static Target' Object!");
            }
        }

        if (DistanceTXT == null)
        {
            try
            {
                DistanceTXT = GameObject.Find("Distance TXT").GetComponent<Text>();
            }
            catch (Exception e)
            {
                Debug.LogWarning("### Zoom Handler Warning | Can't find 'Distance TXT' Object!");
            }
        }

        TrackableStatusDetector.OnTargetDetectionChange += OnTargetDetection;
    }

    private void Start()
    {
        zoomTXT = GameObject.Find("Zoom TXT").GetComponent<Text>();

#if UNITY_IOS
        iosCam = new NatCamDeviceiOS();
        maxIOSZoom = iosCam.MaxZoomRatio(0);
#endif
    }

    void Update()
    {

		distanceFromStage = Vector3.Distance(ArCamera.position, TargetImage.position);
		if (DistanceTXT != null) DistanceTXT.text = "Distance: " + distanceFromStage.ToString("F2");

        if (Input.GetMouseButtonDown(0))
        {
            OnPointerDown();
        }

        if (Input.GetMouseButtonUp(0))
        {
            OnPointerUp();
        }

        if (mouseDown && (Time.unscaledTime - startTapTime) > 0.5f)
        {

#if UNITY_IOS
            currentZoom += 0.1f;
#else
             currentZoom += 0.25f;
#endif
            SetZoom(currentZoom);
        }
    }


    void SetAutoZoom(){

        if(!TargetVisible){
            WaitingForAutoZoom = true;
            return;
        }

        StopCoroutine("AutoZoom");
        StartCoroutine("AutoZoom");
	}

    IEnumerator AutoZoom(){
        // Reset zoom to know the distance
#if UNITY_IOS
		SetZoom(1);

#else
        SetZoom(0);
#endif

        yield return weof;
        yield return weof;
        yield return weof;
        yield return weof;
        yield return weof;

		// Set zoom

#if UNITY_IOS
		SetAutoZoomIOS();

#else
        SetAutoZoomANDROID();
#endif
	}

    public void SetAutoZoomIOS()
    {

        distanceFromStage = Vector3.Distance(ArCamera.position, TargetImage.position);
        if (DistanceTXT != null) DistanceTXT.text = "Distance: " + distanceFromStage.ToString("F2");

        if (maxZoomIOS == 0f)
        { // reset zoom to 1 
            SetZoom(1);
            return;
        }

        currentZoom = Utils.Map(distanceFromStage, minDistanceIOS, maxDistanceIOS, 0, maxZoomIOS);

        SetZoom(currentZoom);

    }

    public void SetAutoZoomANDROID()
    {
        distanceFromStage = Vector3.Distance(ArCamera.position, TargetImage.position);
        if (DistanceTXT != null) DistanceTXT.text = "Distance: " + distanceFromStage.ToString("F2");

        if (maxZoomANDROID == 0f)
        { // reset zoom to 0 
            SetZoom(0);
            return;
        }

        currentZoom = Utils.Map(distanceFromStage, minDistanceANDROID, maxDistanceANDROID, 0, maxZoomANDROID);

        SetZoom(currentZoom);
    }


    public void OnPointerDown()
    {
        startTapTime = Time.unscaledTime;
        mouseDown = true;
    }

    public void OnPointerUp()
    {
        mouseDown = false;
        if ((Time.unscaledTime - startTapTime) < 0.5f)
        {
            currentZoom = 0f;
#if UNITY_IOS
            currentZoom = 1f;
#endif
            SetZoom(currentZoom);
        }


    }

    public void SetZoomSettings(float min_distanceANDROID, float max_distanceANDROID, float max_zoomANDROID,
                               float min_distanceIOS, float max_distanceIOS, float max_zoomIOS)
    {
        minDistanceANDROID = min_distanceANDROID;
        maxDistanceANDROID = max_distanceANDROID;
        maxZoomANDROID = max_zoomANDROID;

        minDistanceIOS = min_distanceIOS;
        maxDistanceIOS = max_distanceIOS;
        maxZoomIOS = max_zoomIOS;

        // -----

        SetAutoZoom();
	}

    void SetZoom(float zoom)
    {
        // (int)Mathf.Ceil(currentZoom)


#if UNITY_IOS && !UNITY_EDITOR
        if (zoom < 1f) zoom = 1f;
        else if (zoom > maxIOSZoom) zoom = maxIOSZoom;

		iosCam.SetZoom(0, zoom);
#else
            if (zoom < 0f) zoom = 0f;
            Vuforia.CameraDevice.Instance.SetField("zoom", Mathf.Ceil(currentZoom).ToString());
#endif

        if (zoomTXT != null) zoomTXT.text = "Zoom: " + zoom.ToString("F2");
	}


    void OnTargetDetection(bool visible){
        TargetVisible = visible;
        if(TargetVisible && WaitingForAutoZoom){
            SetAutoZoom();
            WaitingForAutoZoom = false;
        }
    }
}
