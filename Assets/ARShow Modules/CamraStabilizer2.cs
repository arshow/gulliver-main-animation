﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class CamraStabilizer2 : MonoBehaviour
{
	public static CamraStabilizer2 _;

	VuforiaARController vuforia;


	Vector3 PrevPosition;
    Quaternion PrevRotation;

	Vector3 FixedPosition;
	Quaternion FixedRotation;

	Vector3 VuforiaPosition;
	Quaternion VuforiaRotation;


	Vector3 tmpV3;
    float SmoothSpeed = 25f;


	public float PositionTreshold = 0.01f;

	public float RotationTreshold = 0.01f;

	private void Awake()
	{
		_ = this;
	}

	void Start()
	{
		vuforia = VuforiaARController.Instance;
		vuforia.RegisterTrackablesUpdatedCallback(OnTrackablesUpdated);
		vuforia.RegisterVuforiaStartedCallback(OnInitialized);

		
	}

	public void OnTrackablesUpdated()
	{
		if (!this.isActiveAndEnabled) return;

		VuforiaPosition = transform.position;
		VuforiaRotation = transform.rotation;

		transform.rotation = PrevRotation;
		transform.position = PrevPosition;

		if (Mathf.Abs(VuforiaPosition.x - PrevPosition.x) > PositionTreshold ||
		   Mathf.Abs(VuforiaPosition.y - PrevPosition.y) > PositionTreshold ||
		   Mathf.Abs(VuforiaPosition.z - PrevPosition.z) > PositionTreshold ||
		   Mathf.Abs(VuforiaRotation.x - PrevRotation.x) > RotationTreshold ||
		   Mathf.Abs(VuforiaRotation.y - PrevRotation.y) > RotationTreshold ||
		   Mathf.Abs(VuforiaRotation.z - PrevRotation.z) > RotationTreshold ||
           Mathf.Abs(VuforiaRotation.w - PrevRotation.w) > RotationTreshold
		  )
		{
			FixedPosition = VuforiaPosition;
			FixedRotation = VuforiaRotation;
		}


	}

	public void OnInitialized()
	{
		FixedPosition = PrevPosition = transform.position;
		FixedRotation = PrevRotation = transform.rotation;

		
	}

	void LateUpdate()
	{

		transform.position = Vector3.Lerp(transform.position, FixedPosition, Time.deltaTime * SmoothSpeed);
		transform.rotation = Quaternion.Lerp(transform.rotation, FixedRotation, Time.deltaTime * SmoothSpeed);

		//transform.position = FixedPosition;
		//transform.rotation = FixedRotation;

		PrevPosition = transform.position;
		PrevRotation = transform.rotation;

	}



}
