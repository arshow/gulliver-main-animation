﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PutMeIntoTarget : MonoBehaviour {

	GameObject world;
    Vector3 tmpv3;

	void Awake () {
		world = GameObject.Find("The World");
        if(world == null){
            Debug.LogError("'The World' Not Found!!!");
            return;
        }
        tmpv3 = transform.position;
        transform.parent = world.transform;
        transform.position = tmpv3;
	}
}
