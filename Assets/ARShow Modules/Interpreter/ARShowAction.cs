﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class ARShowAction{

    public string Name;
    public string Layer;
    public UnityEvent OnPlay;
    public UnityEvent OnStop;
    public UnityEvent OnPlayLastFrame;


}
