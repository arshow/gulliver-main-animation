﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterpreterDebugUI : MonoBehaviour {

	public InputField input;

	public void SubmitQuery(){
		ARShowCommunicationModule._.HandleCommandString( input.text );
		//input.text = "";
	}
}
