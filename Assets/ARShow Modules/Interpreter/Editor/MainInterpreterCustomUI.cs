﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using System.IO;

[CustomEditor(typeof(MainInterpreter))]
public class MainInterpreterCustomUI : Editor {

	private ReorderableList ActionsList;
	private bool[] ActionsExpandedStatuses;
	private float[] Heights;

    MainInterpreter mainInterpreter;

	private void OnEnable()
	{

		// Init Lists
		ActionsList = new ReorderableList(serializedObject, serializedObject.FindProperty("SpecialActions"), true, true, true, true);

		ActionsExpandedStatuses = new bool[((MainInterpreter)target).SpecialActions.Count];
		Heights = new float[((MainInterpreter)target).SpecialActions.Count];

		// Draw list header
		ActionsList.drawHeaderCallback = (Rect rect) => {
			EditorGUI.LabelField(rect, "Special Actions");
		};


		// Draw "Action" element
		ActionsList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
		{


			var element = ActionsList.serializedProperty.GetArrayElementAtIndex(index);
			rect.y += 2;
			rect.width -= 10;
			float f1w = rect.width;

			float f1x = rect.x;

			EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), "Animation Name");

			EditorGUI.PropertyField(new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight + 1, rect.width, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("Name"), GUIContent.none);

			ActionsExpandedStatuses[index] = EditorGUI.Foldout(new Rect(rect.x + 10, rect.y + EditorGUIUtility.singleLineHeight * 2 + 10, rect.width - 10, EditorGUIUtility.singleLineHeight), ActionsExpandedStatuses[index], "Actions", true);

			Heights[index] = 70;

			if (ActionsExpandedStatuses[index])
			{
				EditorGUI.PropertyField(new Rect(rect.x, rect.y + Heights[index], rect.width, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("OnPlay"), new GUIContent("Play"));
				Heights[index] += EditorGUI.GetPropertyHeight(element.FindPropertyRelative("OnPlay"));

				EditorGUI.PropertyField(new Rect(rect.x, rect.y + 15 + Heights[index], rect.width, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("OnPlayLastFrame"), new GUIContent("Play Last Frame"));
				Heights[index] += EditorGUI.GetPropertyHeight(element.FindPropertyRelative("OnPlayLastFrame"));

				EditorGUI.PropertyField(new Rect(rect.x, rect.y + 30 + Heights[index], rect.width, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("OnStop"), new GUIContent("Stop"));
				Heights[index] += EditorGUI.GetPropertyHeight(element.FindPropertyRelative("OnStop"));

				Heights[index] += 70;
			}


		};

		// Set elements height
		ActionsList.elementHeightCallback = (index) =>
		{
			Repaint();

			if (index > Heights.Length - 1)
			{
                ActionsExpandedStatuses = new bool[((MainInterpreter)target).SpecialActions.Count];
				Heights = new float[((MainInterpreter)target).SpecialActions.Count];

			}

			return Heights[index];
		};

	}



	public override void OnInspectorGUI()
	{

		serializedObject.Update();
		EditorGUILayout.Space();
		EditorGUILayout.Space();

		serializedObject.FindProperty("ShowName").stringValue = EditorGUILayout.TextField("Show Name ", serializedObject.FindProperty("ShowName").stringValue);


		//EditorGUILayout.Space();
		EditorGUILayout.Space();

		ActionsList.DoLayoutList();

		serializedObject.ApplyModifiedProperties();

		EditorGUILayout.Space();
		EditorGUILayout.Space();

		if (GUILayout.Button("Export Show JSON"))
		{
			InterpreterExporter.Start((MainInterpreter)target);
		}

		EditorGUILayout.Space();
		EditorGUILayout.Space();

		//DrawDefaultInspector();
	}

    void UploadToServer(string json){
        mainInterpreter.StartCoroutine( _upload( mainInterpreter.ShowName, json ) );
    }

    IEnumerator _upload( string show_name, string json){



        // ping server, to check if it reachable

        yield return null;


    }

}
