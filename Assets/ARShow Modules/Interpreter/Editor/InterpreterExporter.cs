﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using SimpleJSON;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InterpreterExporter : EditorWindow
{

    static MainInterpreter interpreter;

    static GUIStyle normalText;
    static GUIStyle warningText;
    static GUIStyle errorText;
    static GUIStyle successText;

    static List<GUIMessage> messages;
    static bool initialized = false;

    private void OnEnable()
    {
		// set styles
		normalText = new GUIStyle();
		warningText = new GUIStyle();
		errorText = new GUIStyle();
		successText = new GUIStyle();
		normalText.normal.textColor = Color.white;
		normalText.fontSize = 12;
		warningText.normal.textColor = Color.yellow;
		warningText.fontSize = 12;
		errorText.normal.textColor = Color.red;
		errorText.fontSize = 12;
		successText.normal.textColor = Color.green;
		successText.fontSize = 12;
    }

    private void OnDisable()
    {
        initialized = false;
    }

    void OnGUI()
    {
		GUILayout.Space(20);
       
        if (!initialized){
            GUILayout.Label("   Click 'Export JSON' to Start the export proccess ", warningText);
            return;
        };

        foreach( GUIMessage msg in messages ){
            msg.Draw();
        }
    }


	// ---

	public static void Start(MainInterpreter interpreter)
	{
		messages = new List<GUIMessage>();

		InterpreterExporter.interpreter = interpreter;
		InterpreterExporter window = (InterpreterExporter)EditorWindow.GetWindow<InterpreterExporter>("Interpreter Export", typeof(SceneView));
		window.Show(true);
        initialized = true;

        // scan scenes and build the json
        string json = BuildJSON();
        if (json == ""){
            messages.Add(new GUIMessage(" Can't find 'scene interpreter in any scene. Nothig to export!'", errorText));
            return;
        }else{
            messages.Add(new GUIMessage("Scenes scan successfully completed, JSON created", successText));
            messages.Add(new GUIMessage(" ", normalText));
        }

        // write json to the local file
        bool res = WriteFile(json);
        messages.Add(new GUIMessage(" ", normalText));

        // upload to server
        UploadToserver(json);
	}

	static string BuildJSON()
	{

        messages.Add(new GUIMessage("Interpreter Export Started. Scaning Scenes...", normalText));

		Dictionary<string, Dictionary<string, ARShowLayer>> foundSceneInterpreters = new Dictionary<string, Dictionary<string, ARShowLayer>>();

		// Iterate over all scenes enabled in build settings
		foreach (UnityEditor.EditorBuildSettingsScene S in UnityEditor.EditorBuildSettings.scenes)
		{
			if (S.enabled)
			{
				//string name = S.path.Substring(S.path.LastIndexOf('/') + 1);
				//name = name.Substring(0, name.Length - 6);

				// ignore main scene
				if (S.path.Contains("Main")) continue;

				Scene sc = EditorSceneManager.OpenScene(S.path, OpenSceneMode.Additive);


				GameObject[] scene_root = sc.GetRootGameObjects();

				// Iterate all gameobjects in loaded scene to find the "SceneInterpreter"
				SceneInterpreter sIntr = null;
				Dictionary<string, ARShowLayer> SceneLayers = null;

				foreach (GameObject go in scene_root)
				{
					sIntr = go.GetComponent<SceneInterpreter>();

					// Find "SceneInterpreter" 
					if (sIntr != null)
					{
						SceneLayers = sIntr.NormalizeDataStructure();
						break;
					}
					else
					{
						sIntr = go.GetComponentInChildren<SceneInterpreter>();
						if (sIntr != null)
						{
							SceneLayers = sIntr.NormalizeDataStructure();
							break;
						}
						else
						{
							SceneLayers = null;
						}
					}

				}

				// Add to scenes array if SceneInterpreter was found
				if (SceneLayers != null && sIntr != null)
				{
					foundSceneInterpreters.Add(sIntr.SceneName, SceneLayers);
				}

				Debug.Log("Scene Loaded: " + sc.name + " | Interpreter found: " + (sIntr != null).ToString());
				
                if( sIntr != null ){
                    messages.Add(new GUIMessage(" - Scene [" + sc.name + "] scaned. | 'scene interpreter' found!", normalText));
                }else{
					messages.Add(new GUIMessage(" - Scene [" + sc.name + "] scaned. | No 'scene interpreter' found", warningText));
				}

                EditorSceneManager.CloseScene(sc, true);

			}
		}

        if( foundSceneInterpreters.Count == 0 ){
            return "";
        }

		//return;
		// ------------
		// --------------------------------------------------- Build Ino object

		// Create INO Object and set show name
		var INO = new InoMainClass();
		INO.showName = InterpreterExporter.interpreter.ShowName;

		// Fill Special Actions
		INO.specialActions = new InoActionClass[InterpreterExporter.interpreter.SpecialActions.Count];
		for (int i = 0; i < InterpreterExporter.interpreter.SpecialActions.Count; i++)
		{
			INO.specialActions[i] = new InoActionClass(InterpreterExporter.interpreter.SpecialActions[i].Name, "special-" + i + ".jpg");
		}

		// Fill Scenes
		INO.scenes = new InoSceneClass[foundSceneInterpreters.Count];
		int scene_index = 0;
		foreach (KeyValuePair<string, Dictionary<string, ARShowLayer>> scene in foundSceneInterpreters)
		{
			// Create INO Scene
			INO.scenes[scene_index] = new InoSceneClass();
			INO.scenes[scene_index].name = scene.Key; // set scene name

			// Fill INO Scene Layers
			INO.scenes[scene_index].layers = new InoLayerlass[scene.Value.Count];
			int layer_index = 0;
			foreach (KeyValuePair<string, ARShowLayer> scene_layer in scene.Value)
			{
				// create INO Layer
				INO.scenes[scene_index].layers[layer_index] = new InoLayerlass();


				INO.scenes[scene_index].layers[layer_index].name = scene_layer.Value.LayerName; // set layer name

				// fill INO Layer actions
				INO.scenes[scene_index].layers[layer_index].actions = new InoActionClass[scene_layer.Value.Actions.Count];
				int action_index = 0;
				foreach (ARShowAction layer_action in scene_layer.Value.Actions)
				{
					INO.scenes[scene_index].layers[layer_index].actions[action_index] = new InoActionClass(layer_action.Name, (scene_index + 1) + "-" + (layer_index + 1) + "-" + (action_index + 1) + ".jpg");
					action_index++;
				}
				layer_index++;
			}
			scene_index++;
		} // end of INO Scenes iteration

		// convert to JSON
		return JsonUtility.ToJson(INO);

	}


    static bool WriteFile(string json){

        messages.Add(new GUIMessage("Writing JSON to local file...", normalText));

		// write JSON file
        try{
			StreamWriter jsonfile = new StreamWriter("Assets/ARShow Modules/Interpreter Exports/ino.json");
			jsonfile.WriteLine(json);
			jsonfile.Close();
			jsonfile.Dispose();
            AssetDatabase.Refresh();
            messages.Add(new GUIMessage("JSON successfully saved as: /Assets/Interpreter Exports/ino.json", successText));
            return true;
        }catch(Exception e){
            messages.Add(new GUIMessage("Error saving JSON file | MSG: " + e.Message, errorText));
        }

        return false;
    }

    static void UploadToserver(string json){
        interpreter.StartCoroutine(_upload(interpreter.ShowName, json));
    }

	static IEnumerator _upload(string show_name, string json)
	{
        messages.Add(new GUIMessage("Checking server reachabillity...", normalText));
        string ip;
        ARShowSettings ars;
        GameObject am = GameObject.Find("ARShow Modules");
        if( am == null ){
            messages.Add(new GUIMessage("Can't find 'ARShow Modules' Object to get a server IP", errorText));
            yield break;
        }else{
            ars = am.GetComponent<ARShowSettings>();
            if( ars == null ){
				messages.Add(new GUIMessage("Can't find 'ARShowSettings' componenet to get a server IP", errorText));
				yield break;
            }
        }

        ip = ars.AutoConnectIP;

		// ping server, to check if it reachable
        messages.Add(new GUIMessage("Pinging server at ["+ip+"]...", normalText));
		Ping p = new Ping(ip);

        yield return p;
     
        if (p.time >= 0)
        {
            messages.Add(new GUIMessage("Ping [" + ip + "] successfully done. (time="+p.time+")", successText));
        }
        else
        {
            messages.Add(new GUIMessage("Ping [" + ip + "] Failed! Server unreachable", errorText));
            yield break;
        }

		// ----- UPLOAD

		
        messages.Add(new GUIMessage("Sending JSON...", normalText));

        if (ip == "192.168.1.2") ip = "arserver.dev";

        WWW request = new WWW("http://"+ip+"/phpserver/posthandler.php", new System.Text.UTF8Encoding().GetBytes(json));

        yield return request;

        if( request.error != null && request.error != ""){
            messages.Add(new GUIMessage("Error uploading JSON. | " + request.error, errorText));
        }else{
            
            Debug.Log(request.text);
            JSONNode res = JSON.Parse(request.text);

            if( res["status"].Value == "error" ){
                messages.Add(new GUIMessage("Error uploading JSON. | " + res["message"].Value, errorText));
            }else{
                messages.Add(new GUIMessage("Server Response: " + res["message"].Value, normalText));
                messages.Add(new GUIMessage("JSON Upload Successfully Complete.", successText));
            }
           
        }

	}

}

class GUIMessage{
    string message;
    GUIStyle style;

    public GUIMessage(string message, GUIStyle style){
        this.message = message;
        this.style = style;
    }

    public void Draw(){
        GUILayout.Label("  > " + message, style);
    }
}


[System.Serializable]
public class InoMainClass
{
	public string showName;
	public InoActionClass[] specialActions;
	public InoSceneClass[] scenes;
}

[System.Serializable]
public class InoSceneClass
{
	public string name;
	public InoLayerlass[] layers;
}

[System.Serializable]
public class InoLayerlass
{
	public string name;
	public InoActionClass[] actions;
}

[System.Serializable]
public class InoActionClass
{
	public string name;
	public string img;

	public InoActionClass(string name, string img)
	{
		this.name = name;
		this.img = img;
	}

	public InoActionClass()
	{
	}
}
