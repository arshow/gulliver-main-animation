﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor(typeof(SceneInterpreter))]
public class SceneInterpreterCustomUI : Editor {

	private ReorderableList ActionsList;
	private bool[] ActionsExpandedStatuses;
	private float[] Heights;

    private void OnEnable()
    {

        // Init Lists
        ActionsList = new ReorderableList(serializedObject, serializedObject.FindProperty("Actions"), true, true, true, true);

		ActionsExpandedStatuses = new bool[((SceneInterpreter)target).Actions.Count];
		Heights = new float[((SceneInterpreter)target).Actions.Count];

        // Draw list header
		ActionsList.drawHeaderCallback = (Rect rect) => {
			EditorGUI.LabelField(rect, "Scene Animations");
		};


        // Draw "Action" element
		ActionsList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
		{


			var element = ActionsList.serializedProperty.GetArrayElementAtIndex(index);
			rect.y += 2;
			rect.width -= 10;
			float f1w = rect.width * 0.85f;
            float f2w = rect.width * 0.15f;

			float f1x = rect.x;
			float f2x = f1x + f1w + 5;

			EditorGUI.LabelField(new Rect(f1x, rect.y, f1w, EditorGUIUtility.singleLineHeight), "Animation Name");
			EditorGUI.LabelField(new Rect(f2x, rect.y, f2w, EditorGUIUtility.singleLineHeight), "Layer");

			EditorGUI.PropertyField(new Rect(f1x, rect.y + EditorGUIUtility.singleLineHeight + 1, f1w, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("Name"), GUIContent.none);
			EditorGUI.PropertyField(new Rect(f2x, rect.y + EditorGUIUtility.singleLineHeight + 1, f2w, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("Layer"), GUIContent.none);

			rect.width += 15;
			ActionsExpandedStatuses[index] = EditorGUI.Foldout(new Rect(rect.x + 10, rect.y + EditorGUIUtility.singleLineHeight * 2 + 10, rect.width - 10, EditorGUIUtility.singleLineHeight), ActionsExpandedStatuses[index], "Actions", true);

			Heights[index] = 70;

			if (ActionsExpandedStatuses[index])
			{
				EditorGUI.PropertyField(new Rect(rect.x, rect.y + Heights[index], rect.width, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("OnPlay"), new GUIContent("Play"));
				Heights[index] += EditorGUI.GetPropertyHeight(element.FindPropertyRelative("OnPlay"));

				EditorGUI.PropertyField(new Rect(rect.x, rect.y + 15 + Heights[index], rect.width, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("OnPlayLastFrame"), new GUIContent("Play Last Frame"));
				Heights[index] += EditorGUI.GetPropertyHeight(element.FindPropertyRelative("OnPlayLastFrame"));

				EditorGUI.PropertyField(new Rect(rect.x, rect.y + 30 + Heights[index], rect.width, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("OnStop"), new GUIContent("Stop"));
				Heights[index] += EditorGUI.GetPropertyHeight(element.FindPropertyRelative("OnStop")) ;

                Heights[index] += 70;
			}


		};

        // Set elements height
		ActionsList.elementHeightCallback = (index) =>
		{
			Repaint();

			if (index > Heights.Length - 1)
			{
				ActionsExpandedStatuses = new bool[((SceneInterpreter)target).Actions.Count];
				Heights = new float[((SceneInterpreter)target).Actions.Count];

			}

			return Heights[index];
		};

	}




	public override void OnInspectorGUI()
	{

		serializedObject.Update();
		EditorGUILayout.Space();
		EditorGUILayout.Space();

		serializedObject.FindProperty("SceneName").stringValue = EditorGUILayout.TextField("Scene Name ", serializedObject.FindProperty("SceneName").stringValue);


		//EditorGUILayout.Space();
		EditorGUILayout.Space();

		ActionsList.DoLayoutList();

		serializedObject.ApplyModifiedProperties();

		EditorGUILayout.Space();
		EditorGUILayout.Space();

		//ARShowInterpreter intptr = (ARShowInterpreter)target;
		//if (GUILayout.Button("Export Show Setup"))
		//{
		//	intptr.ExportShowToJSON();
		//}

		EditorGUILayout.Space();
		EditorGUILayout.Space();

        //DrawDefaultInspector();
	}


}
