﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainInterpreter : MonoBehaviour {

    public static MainInterpreter _;
    public enum ActionTypes { PLAY, STOP, PLAY_LAST_FRAME }
    public string ShowName;
    public List<ARShowAction> SpecialActions;

    // -----

    Transform TheWorldObject;
    SceneInterpreter currentInterpreter;
    WaitForEndOfFrame weof;
    ActionTypes actType;

    private void Awake()
    {
        _ = this;

        weof = new WaitForEndOfFrame();

		if (TheWorldObject == null){
			try{
				TheWorldObject = GameObject.Find("The World").transform;
			}
			catch (Exception e){
				Debug.LogError("### Main Interpreter Error | Can't find 'The World' Object!");
			}
		}

	}

    private void Start()
    {
        // Load the first scene
        SwitchScene(0);
    }

    ActionTypes GetActionType(int actID){
        if (actID == 0) return ActionTypes.STOP;
        else if (actID == 1) return ActionTypes.PLAY;
        else return ActionTypes.PLAY_LAST_FRAME;
    }

    /*
     *  Launch script/animation from currently opened scene
     */
    public void ExecuteCommand(int layer, int action_index, int actionID){
        if( currentInterpreter == null ){
            Debug.LogError("Interpreter Error: Scene Interpreter NOT FOUND!");
            return;
        }

        currentInterpreter.ExecuteCommand(layer, action_index, GetActionType(actionID));
    }

    /**
     *  Launch animation/script from special action
     */
    public void ExecuteSpecialAction(int action_index, int actionID){
        if (SpecialActions.Count <= action_index) return;

        actType = GetActionType(actionID);

		switch (actType)
		{
		case MainInterpreter.ActionTypes.PLAY:
			{
				SpecialActions[action_index].OnPlay.Invoke();
				break;
			}

		case MainInterpreter.ActionTypes.STOP:
			{
				SpecialActions[action_index].OnStop.Invoke();
				break;
			}
		case MainInterpreter.ActionTypes.PLAY_LAST_FRAME:
			{
				SpecialActions[action_index].OnPlayLastFrame.Invoke();
				break;
			}
		}
    }

    /*
     *  Switch Scene logic
     */
    public void SwitchScene( int sceneIndex ){
        // Remove old object
        currentInterpreter = TheWorldObject.GetComponentInChildren<SceneInterpreter>();
        if (currentInterpreter != null) Destroy(currentInterpreter.gameObject);

        StartCoroutine(SwitchSceneCoroutine(sceneIndex));

    }

    IEnumerator SwitchSceneCoroutine(int sceneIndex){
        yield return weof;

        // Load desired scene
        SceneManager.LoadScene(sceneIndex + 1, LoadSceneMode.Additive);

        yield return weof;

        // Find root animations object with "SceneInterpreter"
        currentInterpreter = null;
		

        while( currentInterpreter == null ){
            currentInterpreter = FindObjectOfType(typeof(SceneInterpreter)) as SceneInterpreter;
            Debug.Log("Searching 'SceneInterpreter'...");
            yield return weof;
        }

        // move root scene object to "The World"
        currentInterpreter.transform.parent = TheWorldObject;

        // reset position of the root object
        //currentInterpreter.transform.localPosition = Vector3.zero;

        // remove the temporary loaded scene
        SceneManager.UnloadSceneAsync(sceneIndex + 1);
    }


    /*
     *  Reset show logic
     */

    public void ResetShow(){

        // Stop all special action
        foreach( ARShowAction act in SpecialActions ){
            act.OnStop.Invoke();
        }

        // Load 1-st scene
        SwitchScene(0);

    }


    /**
     *  Reset all animation in specific layer of current scene
     */
    public void ResetLayer(int layer_index){
		if (currentInterpreter == null)
		{
			Debug.LogError("Interpreter Error: Scene Interpreter NOT FOUND!");
			return;
		}

        currentInterpreter.Resetlayer(layer_index);
    }
}




