﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class ARShowLayer {
	public string LayerName;
	public List<ARShowAction> Actions;

    public ARShowLayer(string name){
        LayerName = name;
    }
}

