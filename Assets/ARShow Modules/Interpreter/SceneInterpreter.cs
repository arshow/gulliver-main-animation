﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SceneInterpreter : MonoBehaviour {

    public static SceneInterpreter _;
    public string SceneName;
    public List<ARShowAction> Actions;

    //-------

    Dictionary<string, ARShowLayer> Layers;
    Dictionary<int, string> LayersIndex;
    string layerName;
	void Awake () {
        _ = this;
        NormalizeDataStructure();

	}

	public Dictionary<string, ARShowLayer> NormalizeDataStructure()
	{
        Layers = new Dictionary<string, ARShowLayer>();
        LayersIndex = new Dictionary<int, string>();

		foreach (ARShowAction act in Actions)
		{
            if (!Layers.ContainsKey(act.Layer))
			{
                Layers.Add(act.Layer, new ARShowLayer(act.Layer));
                Layers[act.Layer].Actions = new List<ARShowAction>();
            }

            Layers[act.Layer].Actions.Add(act);

		}

        // Index layers
        int index = 0;
		foreach (KeyValuePair<string, ARShowLayer> kv in Layers)
		{
            LayersIndex.Add(index, kv.Key);
            index++;
		}

		PrintLayers();
        return Layers;
	}

    void PrintLayers(){
        string res = SceneName + " - Animations Setup: \n\n";
        foreach(KeyValuePair<string, ARShowLayer> kv in Layers ){
            res += kv.Key + ") layer name: "+kv.Value.LayerName+"\n";
            foreach( ARShowAction act in kv.Value.Actions ){
                res += " - " + act.Name+ "\n";
            }
            res += "-------------------------\n";
        }

        //Debug.LogWarning(res);
    }

    string GetLayerNameByIndex(int index){
        return LayersIndex[index];
    }

    // --------

    public void ExecuteCommand(int layer, int action_index, MainInterpreter.ActionTypes actType){
        layerName = GetLayerNameByIndex(layer);

        if (!Layers.ContainsKey(layerName)) return;
        if (Layers[layerName].Actions.Count <= action_index) return;


        switch(actType){
            case MainInterpreter.ActionTypes.PLAY:{
                    Layers[layerName].Actions[action_index].OnPlay.Invoke();
                    break;
                }

            case MainInterpreter.ActionTypes.STOP:{
                    Layers[layerName].Actions[action_index].OnStop.Invoke();
					break;
                }
            case MainInterpreter.ActionTypes.PLAY_LAST_FRAME:
				{
                    Layers[layerName].Actions[action_index].OnPlayLastFrame.Invoke();
					break;
				}
        }

    }

    public void Resetlayer(int layer){
        layerName = GetLayerNameByIndex(layer);

        if (!Layers.ContainsKey(layerName)) return;

        foreach( ARShowAction act in Layers[layerName].Actions  ){
            act.OnStop.Invoke();
        }
    }

}
