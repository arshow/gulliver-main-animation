﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraPositionDebug : MonoBehaviour
{

    public Transform ARCam;
    public Text DebugDataTXT;
    public Text DebugDataTXT2;

    bool allOk = false;

    public void Start(){
        try{
            if (ARCam == null) ARCam = GameObject.Find("ARCamera").transform;
            allOk = true;
        }catch(Exception e){
            Debug.LogError("### CameraPositionDebug Error | Can't find ARCamera Object!");
        }

    }

    private void FixedUpdate()
    {
        if(allOk){
			DebugDataTXT.text = ARCam.position.x + " | " + ARCam.position.y + " | " + ARCam.position.z;
			DebugDataTXT2.text = ARCam.rotation.x + " | " + ARCam.rotation.y + " | " + ARCam.rotation.z + " | " + ARCam.rotation.w; 
        }

    }
}
