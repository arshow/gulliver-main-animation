﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class AlignToTwoTargets : MonoBehaviour {

    public Transform Target_1;
    public Transform Target_2;

	

    Vector3 dir;
    Vector3 mid;

    private void Start()
    {
        // Set Vuforia Camera Focus Mode to "Contonous Auto Focus"
        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }

    void Update () {

		dir = Target_2.position - Target_1.position;
		mid = (dir) / 2.0f + Target_1.position;
		transform.position = mid;
        transform.rotation = Quaternion.FromToRotation(Vector3.right, dir);
	}

}
