﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class SettingsScreen : MonoBehaviour {

    public static bool EyewearMode = false;
	public static bool Stabilization = false;
    public static bool ShowGrid = false;
    public static bool ShowFPS = true;
    public static bool ShowDebug = true;

    public CamraStabilizer2 cameraStabilizer2;


    public GameObject EyewearOffBTN;
    public GameObject EyewearOnBTN;
	public GameObject StabilizationOffBTN;
	public GameObject StabilizationOnBTN;
	public GameObject FpsOffBTN;
	public GameObject FpsOnBTN;
	public GameObject DebugDataOffBTN;
	public GameObject DebugDataOnBTN;
	public GameObject GridOffBTN;
	public GameObject GridOnBTN;

	public Slider PositionSlider2;
	public Slider RotationSlider2;

    public CanvasGroup StabilizationSettingsScreen2;
    public GameObject SetSeatScreen;
    public InputField RowField;
    public InputField SeatField;

	public Text PositionValue2;
	public Text RotationValue2;

    public GameObject FPSCounterObj;

    // Server UI
    public Text ServerOfflineLabel;
    public Text ServerOnlineLabel;
    public Text ServerConnectingLabel;
    public GameObject ConnectToServerBOX;
    public GameObject LastCommandBox;
    public Text LastCommandRaw;
    public Text LastCommandScene;
    public Text LastCommandLayer;
    public Text LastCommandAction;
    public InputField IPInput;

    public GameObject DebugDataObj;
    public GameObject DebugDataObj2;

    WaitForEndOfFrame weof;
    enum StabilizationTypes {NONE, CAMERA_SMOOTH, FILTER_AND_SMOOTH, FILTER_AND_SMOTH_2};
    StabilizationTypes CurrentStabilizationMethod;
    StabilizationTypes PrevStabilizationMethod;

    private void Awake()
    {
        weof = new WaitForEndOfFrame();
        CurrentStabilizationMethod = PrevStabilizationMethod = StabilizationTypes.NONE;
    }


    bool allOk = false;


    public void OnEnable(){
		if (cameraStabilizer2 == null)
		{
			try
			{
				cameraStabilizer2 = GameObject.Find("ARCamera").GetComponent<CamraStabilizer2>();
				allOk = true;
			}
			catch (Exception e)
			{
				Debug.LogError("### Settings Screen Error | Can't find ARCamera / CamraStabilizer2 Object!");
			}
		}

        if( cameraStabilizer2.enabled ){

            StabilizatioOn(true);
        }
        RefreshBTNs();
    }


    void RefreshBTNs(){
		EyewearOnBTN.SetActive(EyewearMode);
		EyewearOffBTN.SetActive(!EyewearMode);
		StabilizationOnBTN.SetActive(Stabilization);
		StabilizationOffBTN.SetActive(!Stabilization);
		FpsOffBTN.SetActive(!ShowFPS);
		FpsOnBTN.SetActive(ShowFPS);
        DebugDataOffBTN.SetActive(!ShowDebug);
        DebugDataOnBTN.SetActive(ShowDebug);
		GridOffBTN.SetActive(!ShowGrid);
	    GridOnBTN.SetActive(ShowGrid);

    }

	void SwitchToEyewear()
	{
		if (CameraDevice.Instance.Stop() && CameraDevice.Instance.Deinit())
		{
			DigitalEyewearARController.Instance.SetEyewearType(DigitalEyewearARController.EyewearType.VideoSeeThrough);
			DigitalEyewearARController.Instance.SetStereoCameraConfiguration(DigitalEyewearARController.StereoFramework.Vuforia);
			DigitalEyewearARController.Instance.SetViewerActive(true, true);
		}
	}

	void SwitchToFullscreen()
	{
		if (CameraDevice.Instance.Stop() && CameraDevice.Instance.Deinit())
		{
			DigitalEyewearARController.Instance.SetEyewearType(DigitalEyewearARController.EyewearType.None);
			DigitalEyewearARController.Instance.SetViewerActive(false, true);
		}
	}


    void FadeOutStabilizationWindow( StabilizationTypes mode ){

       StartCoroutine(ToggleStabilizationSettings2(false));
    }

	void FadeInStabilizationWindow()
	{
		StartCoroutine(ToggleStabilizationSettings2(true));

		
	}


   

	IEnumerator ToggleStabilizationSettings2(bool _on)
	{


        if (_on){
			PositionValue2.text = cameraStabilizer2.PositionTreshold.ToString();
			PositionSlider2.value = cameraStabilizer2.PositionTreshold;

			RotationValue2.text = cameraStabilizer2.RotationTreshold.ToString();
			RotationSlider2.value = cameraStabilizer2.RotationTreshold;
            StabilizationSettingsScreen2.gameObject.SetActive(true);
        }
		for (float i = 0f; i < 1f; i += Time.deltaTime * 5f)
		{
			if (!_on) StabilizationSettingsScreen2.alpha = Mathf.Lerp(1f, 0f, i);
			else StabilizationSettingsScreen2.alpha = Mathf.Lerp(0f, 1f, i);
			yield return weof;
		}

        if (_on) StabilizationSettingsScreen2.alpha = 1f;
        else { 
            StabilizationSettingsScreen2.alpha = 0f; 
            StabilizationSettingsScreen2.gameObject.SetActive(false);
        }

	}


    // -----------

   

	public void OnPositionTresholdChanged2(float val)
	{
		PositionValue2.text = "0" + val.ToString("##.###");
		cameraStabilizer2.PositionTreshold = val;
	}

	public void OnRotationTresholdChanged2(float val)
	{
		RotationValue2.text = "0" + val.ToString("##.###");
		cameraStabilizer2.RotationTreshold = val;
	}


    public void EyewearModeOn(bool _on){
        SettingsScreen.EyewearMode = _on;
        RefreshBTNs();

        if (_on) SwitchToEyewear();
        else SwitchToFullscreen();
    }

    public void StabilizatioOn(bool _on){
        SettingsScreen.Stabilization = _on;
        RefreshBTNs();

		cameraStabilizer2.enabled = _on;


		StopAllCoroutines();

        StartCoroutine(ToggleStabilizationSettings2(_on));
		
    }

    public void ShowFPSOn(bool _on){
        SettingsScreen.ShowFPS = _on;
        RefreshBTNs();

        FPSCounterObj.SetActive(_on);
    }

	public void ShowGridOn(bool _on)
	{
        SettingsScreen.ShowGrid = _on;
		RefreshBTNs();

        if (_on) DebugHandler._.GridOn();
        else DebugHandler._.GridOff();

        //if(Grid != null) Grid.SetActive(_on);
	}

	public void ShowDebugOn(bool _on)
	{
        SettingsScreen.ShowDebug = _on;
		RefreshBTNs();

        DebugDataObj.SetActive(_on);
        DebugDataObj2.SetActive(_on);
	}

    public void ConnectToServer(){
        //IPInput.text
        if (string.IsNullOrEmpty(IPInput.text)) return;
        ConnectionHandler._.Connectmanualy(IPInput.text);
    }

    public void SetServerAsNotConnected(){
        ServerOfflineLabel.gameObject.SetActive(true);
        ServerOnlineLabel.gameObject.SetActive(false);
        ServerConnectingLabel.gameObject.SetActive(false);
		ConnectToServerBOX.SetActive(true);
	    LastCommandBox.SetActive(false);
    }

    public void SetServerAsConnecting(){
		ServerOfflineLabel.gameObject.SetActive(false);
		ServerOnlineLabel.gameObject.SetActive(false);
		ServerConnectingLabel.gameObject.SetActive(true);
		ConnectToServerBOX.SetActive(false);
		LastCommandBox.SetActive(false);
    }

	public void SetServerAsConnected(string ip)
	{
        ServerOnlineLabel.text = "Connected (" + ip + ")";
		ServerOfflineLabel.gameObject.SetActive(false);
		ServerOnlineLabel.gameObject.SetActive(true);
		ServerConnectingLabel.gameObject.SetActive(false);
		ConnectToServerBOX.SetActive(false);
		LastCommandBox.SetActive(true);
	}

    public void OnOpenSetSeatClicked(){
        SetSeatScreen.SetActive(true);
    }

	public void OnCloseSetSeatClicked()
	{

        ARShowCommunicationModule._.SendSeatAndRow( RowField.text, SeatField.text );
		SetSeatScreen.SetActive(false);
	}

    string[] command;

    public void setlastCommand( string comm ){

        if (string.IsNullOrEmpty(comm)) return;

        LastCommandRaw.text = comm;

		command = comm.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
		if (command.Length < 3) return;

        LastCommandScene.text = command[0];
		LastCommandLayer.text = command[1];
		LastCommandAction.text = command[2];
    }

    public void OnResetShowClicked(){
        //ARShowInterpreter._.ResetShow();

        //SceneController._.Restart();
    }

    public void OnAutoplayClicked(){
        //Debug.LogError("Autoplay function not set!");

       // SceneController._.PlayAll();
    }
}
