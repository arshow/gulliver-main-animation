﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class DebugHandler : MonoBehaviour {

    public static DebugHandler _;
    public CanvasGroup SettingsScreen;
    public SettingsScreen settingsSceen;

	WaitForEndOfFrame weof;

    public GameObject StageGrid;

	private void Awake()
	{
        _ = this;
		weof = new WaitForEndOfFrame();

        StageGrid = GameObject.Find("Stage Grid");
        GridOff();
	}

	IEnumerator TogglSettingsScreen(bool _on)
	{

        SettingsScreen.gameObject.SetActive(true);

        for (float i = 0f; i < 1f; i += Time.deltaTime * 5f)
		{
			if (!_on) SettingsScreen.alpha = Mathf.Lerp(1f, 0f, i);
			else SettingsScreen.alpha = Mathf.Lerp(0f, 1f, i);
			yield return weof;
		}

		if (_on) SettingsScreen.alpha = 1f;
		else SettingsScreen.alpha = 0f;

		if (!_on) SettingsScreen.gameObject.SetActive(false);

	}

    public void GridOff(){
        if (StageGrid != null) StageGrid.SetActive(false);
    }

    public void GridOn(){
        if (StageGrid != null) StageGrid.SetActive(true);
    }
    // ---------

    public void ShowSettingsScreen(){
        StartCoroutine(TogglSettingsScreen(true));

  //      // VUFORIA ADVANCED API
		//string focusMode = "";
  //      Vuforia.CameraDevice.Instance.GetField("zoom", out focusMode);
		//Debug.Log("##### ##### ##### ##### #####");
		//Debug.Log("##### FocusMode: " + focusMode);

		//// Get the fields
		//IEnumerable cameraFields = Vuforia.CameraDevice.Instance.GetCameraFields();

		//// Print fields to device logs
		//foreach (Vuforia.CameraDevice.CameraField field in cameraFields)
		//{
		//	Debug.Log("Key: " + field.Key + "; Type: " + field.Type);
		//}
    }

    public void OnZoomChanged(float val){
        Vuforia.CameraDevice.Instance.SetField("zoom", val.ToString());
    }

    public void HideSettingsScreen(){
        StartCoroutine(TogglSettingsScreen(false));
    }

    // ----------

    public void OnServerConnected(string ip){
        settingsSceen.SetServerAsConnected(ip);
    }

    public void OnServerDisconnected(){
        settingsSceen.SetServerAsNotConnected();
    }

    public void OnConnectingToServer(){
        settingsSceen.SetServerAsConnecting();
    }

    public void OnCammandRecieved(string comm){
        settingsSceen.setlastCommand(comm);
    }

	// ------

	public void TriggerAutoFocus()
	{
		bool focusApplied = CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO);
		Debug.Log("IS AUTO FOCUS TRIGGERED: " + focusApplied.ToString());
	}

}
