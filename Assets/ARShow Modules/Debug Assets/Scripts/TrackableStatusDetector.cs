﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Vuforia;

public class TrackableStatusDetector : MonoBehaviour, ITrackableEventHandler
{
    private TrackableBehaviour mTrackableBehaviour;
    public static bool IsTracking = false;
    public static Action<bool> OnTargetDetectionChange;


    void Start()
	{
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}

		var vuforia = VuforiaARController.Instance;
		vuforia.RegisterVuforiaStartedCallback(OnVuforiaStarted);
	}

	public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus,TrackableBehaviour.Status newStatus)
	{
        

		if (newStatus == TrackableBehaviour.Status.DETECTED || newStatus == TrackableBehaviour.Status.TRACKED )
		{
            // Target Detected 
            IsTracking = true;
            ARShowCommunicationModule._.SendTrackingStatus(true);
            if (OnTargetDetectionChange != null) OnTargetDetectionChange(true);

		}
		else
		{
            // Target Lost
            IsTracking = false;
            ARShowCommunicationModule._.SendTrackingStatus(false);
            if (OnTargetDetectionChange != null) OnTargetDetectionChange(false);
		}
	}

	private void OnVuforiaStarted()
	{
		bool focusApplied = CameraDevice.Instance.SetFocusMode( CameraDevice.FocusMode.FOCUS_MODE_INFINITY);
        Debug.Log("IS INFINITY FOCUS APPLIED: " + focusApplied.ToString());
	}

    public void TriggerAutoFocus(){
		bool focusApplied = CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO);
		Debug.Log("IS AUTO FOCUS TRIGGERED: " + focusApplied.ToString());
    }

}

