﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FPSCounter : MonoBehaviour {

	public Text output;
	float deltaTime = 0f;
	int fps = 0;

	int[] fpsBuffer = {
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
	};

	int bufferIndex = 0;

	void Update () {
		
		deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
		fpsBuffer [bufferIndex] = (int)(1f / deltaTime);
		bufferIndex++;
		if (bufferIndex > fpsBuffer.Length - 1) bufferIndex = 0;


	}

	void FixedUpdate(){

		fps = 0;
		foreach (int val in fpsBuffer) {
			fps += val;
		}
		fps = fps / fpsBuffer.Length;

		output.text = fps + " FPS";
	}
}
