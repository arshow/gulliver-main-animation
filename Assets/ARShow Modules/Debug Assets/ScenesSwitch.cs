﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesSwitch : MonoBehaviour {

    public void GoToMainScene(){
        SceneManager.LoadScene("Main");
    }

	public void GoToScene1()
	{
        SceneManager.LoadScene("test_scene1", LoadSceneMode.Additive);
	}
}
