﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spaceshipController : MonoBehaviour {

    float move = 0f;
    float rotate = 0f;
    float vertical = 0f;

	void Awake () {
        // Subscribe to keyboard events from pinocchio panel
        ARShowCommunicationModule.OnKeyUp += OnKeyUp;
        ARShowCommunicationModule.OnKeyDown += OnKeyDown;
	}
	
	
    void OnKeyUp(ARShowCommunicationModule.KeyName kName){
        
        switch( kName ){
            case ARShowCommunicationModule.KeyName.UP: 
            case ARShowCommunicationModule.KeyName.DOWN : {
                    vertical = 0f;
                    break;
                }

            case ARShowCommunicationModule.KeyName.LEFT:
            case ARShowCommunicationModule.KeyName.RIGHT:{
                    rotate = 0f;
                    break;
                }

            case ARShowCommunicationModule.KeyName.FORWARD:
            case ARShowCommunicationModule.KeyName.BACKWARD:{
                    move = 0f;
                    break;
                }
        }


    }

	void OnKeyDown(ARShowCommunicationModule.KeyName kName){

		switch (kName)
		{
            case ARShowCommunicationModule.KeyName.UP:{
                    vertical = 1f;
                    break;
                }
			case ARShowCommunicationModule.KeyName.DOWN:
				{
					vertical = -1f;
					break;
				}

            case ARShowCommunicationModule.KeyName.LEFT:{
                    rotate = -1f;
                    break;
                }
			case ARShowCommunicationModule.KeyName.RIGHT:
				{
					rotate = 1f;
					break;
				}

            case ARShowCommunicationModule.KeyName.FORWARD:{
                    move = 1f;
                    break;
                }
			case ARShowCommunicationModule.KeyName.BACKWARD:
				{
					move = -1f;
					break;
				}
		}


	}

	void Update () {

        if( move != 0f ){
            transform.Translate(Vector3.forward * move * Time.deltaTime * 2f);
        }

        if( vertical != 0f ){
            transform.Translate(Vector3.up * vertical * Time.deltaTime * 2f);
        }

        if( rotate != 0f ){
            transform.Rotate(Vector3.up * rotate * Time.deltaTime * 100f);
        }

	}
}
