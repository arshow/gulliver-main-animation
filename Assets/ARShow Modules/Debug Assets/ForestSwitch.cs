﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForestSwitch : MonoBehaviour {

    public GameObject OnBTN;
    public GameObject OffBTN;
    public GameObject Forest;


	void Start () {
        OnBTN.SetActive(true);
        OffBTN.SetActive(false);
	}
	
    public void TurnOn(){
		OnBTN.SetActive(true);
		OffBTN.SetActive(false);
        Forest.SetActive(true);
    }

    public void TurnOff(){
		OnBTN.SetActive(false);
		OffBTN.SetActive(true);
		Forest.SetActive(false);
    }
}
