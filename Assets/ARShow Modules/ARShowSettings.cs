﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARShowSettings : MonoBehaviour {

    public static ARShowSettings _;

    public bool IsMonitor = false;
    public string AutoConnectIP = "";
	public int UDPIncommingPort = 3004;
	public int UDPOutgoungPort = 3003;

    private void Awake()
    {
        _ = this;
    }


}
