﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldHandler : MonoBehaviour {

    public static WorldHandler _;

    Vector3 originalPosition;
    Vector3 offset;

	void Awake () {
        _ = this;
        originalPosition = transform.localPosition;
        offset = Vector3.zero;
	}
	
    public void SetOffset(float x, float y, float z){
        offset.x = x;
        offset.y = y;
        offset.z = z;

        transform.localPosition = originalPosition + offset;
    }
}
