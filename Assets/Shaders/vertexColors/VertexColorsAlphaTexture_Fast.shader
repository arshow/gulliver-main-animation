﻿Shader "Vertex Colors/Terrain Scenery Shader"{
	Properties
	{
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	}

	Category
	{

		ZWrite Off //to get rid of artifacts
   	    AlphaTest Greater 0.0

   	   
		Tags { "Queue"="Transparent" }
		Lighting Off
		BindChannels
		{
			Bind "Color", color
			Bind "Vertex", vertex
			Bind "TexCoord", texcoord
		}

		Blend SrcAlpha OneMinusSrcAlpha


		SubShader
		{
			Pass
			{
				SetTexture [_MainTex]
				{
					Combine texture * primary
				}
			}
		}
	}
}