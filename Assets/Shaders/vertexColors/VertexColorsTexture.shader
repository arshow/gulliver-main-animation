﻿Shader "Vertex Colors/Texture" {
   Properties {
      _MainTex ("Base (RGB)", 2D) = "white"
   }
   SubShader {
   	  ZWrite Off
      ZTest Always
      Pass {
         SetTexture [_MainTex]
      }
   }
}