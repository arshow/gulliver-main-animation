﻿Shader "Vertex Colors/Additive Alpha Texture" {


Properties {
    _MainTex ("Texture", 2D) = ""
}


SubShader {
    Tags {"Queue" = "Transparent"}
    Blend One One
    ZWrite Off
   // AlphaTest Greater 0.0
    Pass {
        SetTexture[_MainTex]{	
        	Combine texture * primary
        }
    } 
}


}

