﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipsController : MonoBehaviour {



	public float FleetState = 0;


	Animator[] ships;
	int currentState = 0;

	// Use this for initialization
	void Start () {
		ships = GetComponentsInChildren<Animator> ();
		foreach (var ship in ships) {
			ship.speed = Random.Range (0.9f,1.05f);
			var euler = ship.transform.eulerAngles;
			euler.y += Random.Range (-40f,40f);
			ship.transform.rotation = Quaternion.Euler (euler);
			ship.Play ("Idle");
		}
	}


	void Update () {


		var newState = (int)Mathf.Floor (FleetState);
		if (newState != currentState) {
			currentState = newState;
			foreach (var ship in ships) {
				ship.SetInteger ("State",newState);
			}
		}




	}












}
