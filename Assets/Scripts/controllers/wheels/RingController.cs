﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;





public class RingController : MonoBehaviour {

	/*
	class Pos{
		public Vector3 position;
		public Quaternion rotation;
	}
	*/

	const string WATERFALL = "WATERFALL";
	const string TEMPLE = "TEMPLE";
	const string LIGHTHOUSE = "LIGHTHOUSE";

	const float DEFAULT_SPEED = 3.27f;

	public WheelController ctrl;
	public RotateTransform rotator;
	public Animator waterfall;
	public Animator temple;
	public Animator lighthouse;

	public float radius = 1f;


	public GameObject lighthousePoses;
	public GameObject waterfallPoses;
	public GameObject templePoses;

	List<GameObject> _objects;

	//Dictionary<GameObject,Pos> _poses;


	//public List<Transform> poses;
	//public List<Transform> gos;
	//public GameObject t;


	//put an array of position gameobjects, feed it to wheel controller
	//coruotine waits for the first hidden pos gameobject that's deactivated/has no children
	//when found, parent target to pos or just copy its position and rotation/look at pivot

	//on/off animation

	//mark objects in stateMap. turnOn/Off only sets map. update does the rest


	//when triggered on, look for matching hidden pos, turn on.
	//when slice gets out,if go is on, mark it for off
	//alt, when slice goes hidden, hide go in it

	void Awake(){

		/*
		_poses = new Dictionary<GameObject, Pos> ();
		_poses [lighthouse.gameObject] = new Pos (){ 
			position = lighthouse.transform.position, rotation = lighthouse.transform.rotation 
		};
		*/

		_objects = new List<GameObject> ();

		put (waterfallPoses,WATERFALL);
		put (templePoses,TEMPLE);
		put (lighthousePoses,LIGHTHOUSE);

		lighthouse = null;
		temple = null;
		waterfall = null;


	}

	void put(GameObject source,string tag){
		foreach (Transform t in source.transform) {
			_objects.Add (t.gameObject);
			t.name = tag;
		}


	}



	void OnEnable(){
		ARShowCommunicationModule.OnKeyDown += onKeyDown;
	}

	void OnDisable(){
		ARShowCommunicationModule.OnKeyDown -= onKeyDown;
	}


	void onKeyDown(ARShowCommunicationModule.KeyName key){
		
		if (rotator == null)
			return;
		

		var speed = rotator.rotationVelocity.z;

		var step = 1f;

		if (key == ARShowCommunicationModule.KeyName.UP) {

			speed += step;

		}else if (key == ARShowCommunicationModule.KeyName.DOWN) {

			speed -= step;

		}


		speed = Mathf.Clamp (speed, 0, 25f);

		var rot = rotator.rotationVelocity;
		rot.z = speed;
		rotator.rotationVelocity = rot;

	}



	void Start(){

		//var objects = new GameObject[]{waterfall.gameObject,temple.gameObject,lighthouse.gameObject};
		//ctrl.SetContent(objects);


		hideAll ();



	}



	void hideAll(){

		/*
		waterfall.gameObject.SetActive(false); 
		temple.gameObject.SetActive(false); 
		lighthouse.gameObject.SetActive(false); 
		*/

		foreach(var o in _objects){
			o.SetActive (false);
		}


	}

	public void ResetState(){
		hideAll ();


		if (ctrl.contentCount == 0) {


			//var objects = new GameObject[]{ waterfall.gameObject, temple.gameObject, lighthouse.gameObject };
			//ctrl.SetContent (objects);

			ctrl.SetContent (_objects);

		}

		ctrl.onHidden = onHidden;


		rotator.rotationVelocity.z = DEFAULT_SPEED;

	}


	void onHidden(WheelController.Slice slice){
		var objects = ctrl.GetSliceContent (slice);
		if (objects == null)return;
	
		//Debug.Log ("onHidden:");


		foreach (var o in objects) {

			if (o.activeSelf) {

				o.SetActive (false);

				if (waterfall != null && o == waterfall.gameObject)waterfall = null;
				if (temple != null && o == temple.gameObject)temple = null;
				if (lighthouse != null && o == lighthouse.gameObject)lighthouse = null;



			//	Debug.Log ("Reseting:"+o);

			}





		}
	}


	public void WaterfallAction(){
		if (waterfall != null) {
			waterfall.Play ("Action");
		}
	}

	public void TempleAction(){
		if (temple != null) {
			temple.Play ("Action");
		}
	}
		
	public void LighthouseAction(){
		if (lighthouse != null) {
			lighthouse.Play ("Action");
		}
	}



	public void Waterfall(){




		//waterfall.Play ("Idle");
	//	TurnOn (waterfall.gameObject);

		show (WATERFALL);
	}

	public void Temple(){
	//	TurnOn (temple.gameObject);
	//	temple.Play ("Idle");

		show (TEMPLE);
	}
		
	public void Lighthouse(){
	//	TurnOn (lighthouse.gameObject);
	//	lighthouse.Play ("Idle");

		show (LIGHTHOUSE);
	}


	Animator find(string tag){
		if (tag == WATERFALL)return waterfall;
		if (tag == TEMPLE)return temple;
		if (tag == LIGHTHOUSE)return lighthouse;
		return null;
	}


	void set(string tag,Animator go){
		if (tag == WATERFALL)
			waterfall = go;
		
		if (tag == TEMPLE)
			temple = go;
		
		if (tag == LIGHTHOUSE)
			lighthouse = go;

		go.Play ("Idle");
	}


	void show(string tag){

		var animator = find(tag);

		if (animator != null) {
		//	Debug.Log ("skipping:" +tag + " casue:" + animator);
			return;
		}

		//Debug.Log ("Activating:"+Time.frameCount + " " +tag);
		StartCoroutine (toggle2(tag,true));


	}




	IEnumerator toggle2(string tag,bool state){

		var matched = false;

		while (!matched) {

			ctrl.Apply (false, g => {
				if (g.name == tag) {


			//		Debug.Log("found: "+tag + " " + find(tag));

					if(find(tag) == null){
						set(tag,g.GetComponent<Animator>());
						g.SetActive (state);
					}


					matched = true;
				}
			});

			yield return new WaitForEndOfFrame ();

		}


	}








	void TurnOn(GameObject go){

		if (go.activeSelf) {
			return;
		}

		//Debug.Log ("Activating:"+Time.frameCount + " " +go.name);
		StartCoroutine (toggle(go,true));

	}





	IEnumerator toggle(GameObject go,bool state){

		var matched = false;

		while (!matched) {

			ctrl.Apply (false, g => {
				if (g == go) {
					go.SetActive (state);

					matched = true;
				}
			});

			yield return new WaitForEndOfFrame ();

		}


	}





}
