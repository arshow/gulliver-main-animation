﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


//[RequireComponent(typeof(ctrl))]
public class FrontWheelController : MonoBehaviour {

	public WheelController ctrl;

	public List<GameObject> content;

	// Use this for initialization
	void Start () {


		//var objects = content.SelectMany (o => o.GetComponentsInChildren<Transform> ())
	//		.Select (t=>t.gameObject);

		var objects = content;


		//toggle off all gos
		foreach (var o in objects) {
			o.SetActive(false);
		}

		//set target objects, group them
		ctrl.SetContent(objects);






	}


	void Update(){

		//filter trees, put a counter for each type

		ctrl.Apply (true,go=>{
			//if(!go.activeSelf){
				go.SetActive(false);
		//	}
		});

		ctrl.Apply (false,go=>{
			//if(!go.activeSelf){
				go.SetActive(true);
		//	}
		});

	}





}
