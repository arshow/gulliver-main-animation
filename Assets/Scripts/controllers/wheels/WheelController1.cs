﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;



public class WheelController1 : MonoBehaviour {

	[System.Serializable]
	class Slice{
		public float min;
		public float max;
		public Color color;
		public float length;
		public bool hidden;
	}



	public List<GameObject> content;


	public Transform pivot;
	public Vector3 axis;

	public float baseAngle;
	public int numSlices;

	public float deadMin;
	public float deadMax;
	public Color deadColor;
	public float length;

	//Slice _deadZone;
	Slice[] _slices;

	Dictionary<Slice,List<Renderer>> _content;
	Renderer[] _objects;

	void Start(){

		//_deadZone = new Slice ();

		_slices = new Slice[numSlices];

		var currentAngle = baseAngle;
		var slice = 360f / numSlices;


		var colors = new Color[]{Color.magenta,Color.green,Color.blue,Color.yellow};

		for (int i = 0; i < numSlices; i++) {
		
			var nextAngle = currentAngle + slice;

			var m1 = normalizeAngle (currentAngle,-180f);
			var m2 = normalizeAngle (nextAngle,-180f);

			_slices [i] = new Slice () {
				min = currentAngle, max = nextAngle, color = colors[i % colors.Length]
			};

			currentAngle = nextAngle;

		//	Debug.Log (i + " " + _slices[i].min + " " + _slices[i].max );
		}



		_slices [0].color = Color.black;
		_slices [1].color = Color.gray;



		_objects = content.SelectMany (o=>o.GetComponentsInChildren<Renderer>()).ToArray();
	
		//Debug.Log ("NUMOBjects"+_objects.Count());


		_content = new Dictionary<Slice, List<Renderer>> ();


		foreach (var o in _objects) {

			//get then angle between the object and pivot on the constrained axis
		//	o.enabled = false;

			Vector3 CannonPosition = pivot.position;
			Vector3 SoliderPosition = o.transform.position;

			Vector3 CannonDirection = Vector3.up;

			Vector3 CrossVector1 = CannonDirection;
			Vector3 CrossVector2 = SoliderPosition - CannonPosition;

			CrossVector1.Normalize();
			CrossVector2.Normalize();
			Vector3 CrossResult = Vector3.Cross( CrossVector1,CrossVector2);
			float angle = Sum(Vector3.Scale (CrossResult,axis)) * Mathf.Rad2Deg;


		//	var r = Quaternion.LookRotation (CrossVector2);

		//	angle = Sum (Vector3.Scale (r.eulerAngles, axis));

		//	var angle2 = Vector3.SignedAngle (CrossVector2, CannonDirection, axis);
			//CrossVector2.z = CannonPosition.z;

			//var angle2 = Vector3.Angle (CrossVector2, CannonDirection);

			var diff = (Vector2)(o.transform.position - pivot.position);

			//both are the same:
			 angle = Vector2.SignedAngle (diff, Vector2.up);
			angle = Vector3.SignedAngle (diff, Vector3.up, axis);
			 
			var diff2 = (o.transform.position - pivot.position);
			diff2 = Vector3.Scale (diff2,Negate(axis));
			angle = Vector3.SignedAngle (diff2, Vector3.up, axis);

			//normalize -180 -> 180 range to 0 -> 360 range
			var angle2 = normalizeAngle (angle, 0);
			angle = normalizeAngle(360f - angle2,0);
			//search for the matching slice whose range contains it

			var key = _slices.FirstOrDefault (s=>angle >= s.min && angle < s.max);

		//	Debug.Log (o.name + " : " +angle + ": " + " " + " " +  key);

				
			//get the collider key
		

			if (key == null) {
				throw new UnityException ("no match with " + o.name);
				continue;
			}


			o.material.color = key.color;


			if (!_content.ContainsKey (key)) {
				_content [key] = new List<Renderer> ();
			}

			_content [key].Add (o);


		}








	}










	Vector3 Negate(Vector3 v){
		var n = v;
		n.x = n.x == 0 ? 1f : 0;
		n.y = n.y == 0 ? 1f : 0;
		n.z = n.z == 0 ? 1f : 0;
		return n;
	}




	float Sum(Vector3 v){
		return v.x + v.y + v.z;
	}


	//shift = 0 : 0 - 360
	//shift = -180 : -180 - 180
	float normalizeAngle(float angle,float shift){
		return angle + Mathf.Ceil ((-angle + shift) / 360) * 360;
	}



	void Update(){
		
		//var current = Vector3.Scale (pivot.eulerAngles, axis);


		var current = Sum(Vector3.Scale(pivot.eulerAngles , axis));

		current = normalizeAngle (current, 0);

		for (int i = 0; i < _slices.Length; i++) {
		
			var slice = _slices [i];

			//var min = current + axis * slice.min;
		//	var max = current + axis * slice.max;
			var min = normalizeAngle(current + slice.min,0);
			var max = normalizeAngle(current + slice.max,0);

			if (min > max) {
				var m = max;
				max = min;
				min = m;
			}


			var currentState = slice.hidden;

			slice.hidden = min >= deadMin && max < deadMax;

			if (currentState != slice.hidden) {
			//	toggle (slice, slice.hidden);
				//	Debug.Log (Time.frameCount + " i:"+i + " "  + pivot.eulerAngles.z + " " + current + " " +min + " " + max);

			}




			if (slice.hidden) {
				
				slice.length = length * 1.5f;

			} else {
				
				slice.length = length;

			}

		


		}



	}



	void toggle(Slice slice,bool visible){

		//checks if the slice doesnt contain any elements
		if (_content.ContainsKey (slice)) {
			foreach (var v in _content[slice]) {
				v.enabled = visible;
			}
		}


	}


	void OnDrawGizmos(){


		if (pivot == null)
			return;
		
		Gizmos.DrawWireCube (pivot.position,Vector3.one * 3f);

		drawSlice (deadMin,deadMax,deadColor,length * 1.2f,false);

		if (_slices != null) {
			for (int i = 0; i < _slices.Length; i++) {
				drawSlice (_slices [i].min, _slices [i].max, _slices [i].color,  _slices [i].length,true);
			}
		}


		//var invert =

		if (_objects != null) {
			foreach (var o in _objects) {

				var pos = o.transform.position;

				pos.z = pivot.position.z;

			//	Gizmos.color = Color.red;
			//	Gizmos.DrawLine (pivot.position,pos);


			}
		}

	}




	void drawSlice(float min,float max,Color color,float size,bool local){

		var current = local ? Vector3.Scale (pivot.eulerAngles, axis) : Vector3.zero;

		var v = Vector3.up * size;
		var minPos = pivot.position + Quaternion.Euler (current + axis * min) * v;
		var maxPos = pivot.position + Quaternion.Euler (current + axis * max) * v;


		Gizmos.color = color;
		Gizmos.DrawLine (pivot.position,minPos);
		Gizmos.DrawLine (pivot.position,maxPos);
		Gizmos.DrawLine (minPos,maxPos);

	}



}
