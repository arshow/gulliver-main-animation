﻿using UnityEngine;
using System.Collections;

public class WheelSlice : MonoBehaviour
{

	public Transform pivot;
	public Vector3 axis;
	public float min;
	public float max;
	public float length;
	public Color color;


	void OnDrawGizmos(){

		if (pivot != null) {

			Gizmos.DrawWireCube (pivot.position,Vector3.one * 3f);

			var v = Vector3.up * length;
			var minPos = pivot.position + Quaternion.Euler (axis * min) * v;
			var maxPos = pivot.position + Quaternion.Euler (axis * max) * v;

			Gizmos.color = color;
			Gizmos.DrawLine (pivot.position,minPos);
			Gizmos.DrawLine (pivot.position,maxPos);
			Gizmos.DrawLine (minPos,maxPos);

		}


	}





}

