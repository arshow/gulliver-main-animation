﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class WheelController : MonoBehaviour {

	public enum SliceState{
		Hidden,Visible,JustHidden,JustVisible
	}


	//[System.Serializable]
	public class Slice{
		public float min;
		public float max;
		public bool hidden;
		public Color color;//for debugging
	}


	public Transform pivot;
	public Vector3 axis;

	public float baseAngle;
	public int numSlices;

	public float deadMin;
	public float deadMax;

	public bool flipped;

	public System.Action<Slice> onHidden;
	public System.Action<Slice> onVisible;

	//for debugging
	public Color deadColor;
	public float length;
	public bool debugContent = false;
	public float debugContentSize = 2f;


	public int contentCount{
		get{ 
			return _content == null ? 0 : _content.Count;
		}
	}

	Slice[] _slices;
	Dictionary<Slice,List<GameObject>> _content;



	void Awake(){


		_slices = new Slice[numSlices];

		var currentAngle = baseAngle;
		var slice = 360f / numSlices;

		var colors = new Color[]{Color.magenta,Color.green,Color.blue,Color.yellow};

		for (int i = 0; i < numSlices; i++) {
		
			var nextAngle = currentAngle + slice;

			var c = colors [i % colors.Length];
			c = Random.ColorHSV ();

			_slices [i] = new Slice () {min = currentAngle, max = nextAngle, color = c};

			currentAngle = nextAngle;

		//	Debug.Log (i + " " + _slices[i].min + " " + _slices[i].max );
		}



		_slices [0].color = Color.black;
		_slices [1].color = Color.gray;

	}



	public void SetContent(IEnumerable<GameObject> objects){

		if (_content != null) {
			_content.Clear ();
		} else {
			_content = new Dictionary<Slice, List<GameObject>> ();
		}


		var negative = Negate (axis);

		foreach (var o in objects) {

			var diff = Vector3.Scale(o.transform.position - pivot.position,negative);

			var angle = Vector3.SignedAngle (diff, Vector3.up, axis);

			//normalize -180 -> 180 range to 0 -> 360 range
			angle = normalizeAngle (angle, 0);

			//it goes the other way around of the slices
			angle = normalizeAngle(360f - angle,0);


			//search for the matching slice whose range contains it
			var key = _slices.FirstOrDefault (s=>angle >= s.min && angle < s.max);
			//	Debug.Log (o.name + " : " +angle + ": " + " " + " " +  key);


			if (key == null) {
				throw new UnityException ("no match with " + o.name);
				continue;
			}



			if (!_content.ContainsKey (key)) {
				_content [key] = new List<GameObject> ();
			}

			_content [key].Add (o);

		}



	}




	public List<GameObject> GetSliceContent(Slice slice){
		if (_content.ContainsKey (slice)) {
			return _content [slice];
		}
		return null;
	}


	public void Apply(bool visible,System.Action<GameObject> f){

		foreach (var kv in _content.Keys) {

			if (kv.hidden == !visible) {

				foreach (var o in _content[kv]) {
					f (o);

				}

			}

		}

	}




	void Update(){



		var dmin = normalizeAngle(deadMin,0);
		var dmax = normalizeAngle(deadMax,0);

		if (dmin > dmax) {
			var m = dmax;
			dmax = dmin;
			dmin = m;
		}



		var current = Sum(Vector3.Scale(pivot.eulerAngles , axis));
	
		current = pivot.eulerAngles.z;

		//Debug.Log (pivot.eulerAngles + " " + pivot.localEulerAngles + " "  + current);

		current = normalizeAngle (current, 0);



		if (flipped) {
			current = normalizeAngle (360f - current, 0);
		}


		for (int i = 0; i < _slices.Length; i++) {
		
			var slice = _slices [i];
	
			var min = normalizeAngle(current + slice.min,0);
			var max = normalizeAngle(current + slice.max,0);

			if (min > max) {
				var m = max;
				max = min;
				min = m;
			}


			var currentState = slice.hidden;

			slice.hidden = min >= dmin && max < dmax;

			if (currentState != slice.hidden) {

				if (slice.hidden) {
					if(onHidden != null)onHidden (slice);
				}else {
					if(onVisible != null)onVisible (slice);
				}
				//	Debug.Log (Time.frameCount + " i:"+i + " "  + pivot.eulerAngles.z + " " + current + " " +min + " " + max);
			}

		}



	}




	static float Sum(Vector3 v){
		return v.x + v.y + v.z;
	}


	static Vector3 Negate(Vector3 v){
		var n = v;
		n.x = n.x == 0 ? 1f : 0;
		n.y = n.y == 0 ? 1f : 0;
		n.z = n.z == 0 ? 1f : 0;
		return n;
	}


	//shift = 0 : 0 - 360
	//shift = -180 : -180 - 180
	static float normalizeAngle(float angle,float shift){
		return angle + Mathf.Ceil ((-angle + shift) / 360) * 360;
	}






	void OnDrawGizmosSelected(){


		if (pivot == null)
			return;
		
		Gizmos.DrawWireCube (pivot.position,Vector3.one * 3f);

		drawSlice (deadMin,deadMax,deadColor,length * 1.3f,false);

		if (_slices != null) {
			for (int i = 0; i < _slices.Length; i++) {
				var n = _slices [i].hidden ? length * 1.2f : length;
				drawSlice (_slices [i].min, _slices [i].max, _slices [i].color,n ,true);
			}
		}



		if (debugContent && _content != null) {
			foreach (var kv in _content.Keys) {
				var c = kv.color;
				c.a = 0.8f;
				Gizmos.color = c;
				foreach (var o in _content[kv]) {

					Gizmos.DrawCube (o.transform.position, Vector3.one * debugContentSize);

					/*
					var r = o.GetComponentInChildren<Renderer> ();
					if (r != null) Gizmos.DrawCube (r.bounds.center, r.bounds.size * 1.1f);
					*/
				}
			}
		}



	}




	void drawSlice(float min,float max,Color color,float size,bool local){

		var current = local ? Vector3.Scale (pivot.eulerAngles, axis) : Vector3.zero;

		var v = Vector3.up * size;
		var minPos = pivot.position + Quaternion.Euler (current + axis * min) * v;
		var maxPos = pivot.position + Quaternion.Euler (current + axis * max) * v;


		Gizmos.color = color;
		Gizmos.DrawLine (pivot.position,minPos);
		Gizmos.DrawLine (pivot.position,maxPos);
		Gizmos.DrawLine (minPos,maxPos);

	}




}
