﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class WheelController2 : MonoBehaviour {


	public Collider deadZone;
	public GameObject slices;
	public List<GameObject> content;

	Collider[] _slices;
	Dictionary<Collider,List<Renderer>> _content;

	// Use this for initialization
	void Start () {

		_slices = slices.GetComponentsInChildren<Collider> ();


		foreach (var s in _slices) {
			s.GetComponent<Renderer> ().enabled = false;
		}

		//get all renderers
		var objects = content.SelectMany (o=>o.GetComponentsInChildren<Renderer>());
		Debug.Log ("NUMOBjects"+objects.Count());


		_content = new Dictionary<Collider, List<Renderer>> ();

		foreach (var o in objects) {

			var cols = Physics.OverlapBox (o.bounds.center, o.bounds.extents, o.transform.rotation);

			//get the collider key
			if (cols.Length > 1) {
				throw new UnityException ("this shouldnt happen too many colldiers");
			}

			if (cols.Length == 0)
				continue;

			var col = cols [0];

			var key = _slices.FirstOrDefault (c => c == col);

			if (key == null) {
				//throw new UnityException ("the collider is not a slice:"+col.gameObject.name);
				continue;
			}


			if (!_content.ContainsKey (key)) {
				_content [key] = new List<Renderer> ();
			}

			_content [key].Add (o);

		}




	}


	// Update is called once per frame
	void Update () {

		var bounds = deadZone.bounds;
		var cols = Physics.OverlapBox (deadZone.bounds.center, deadZone.bounds.extents, deadZone.transform.rotation);


		foreach (var s in _slices) {
			//s.GetComponent<Renderer> ().enabled = false;

			toggle (s, false);
		}

		foreach (var s in cols) {
			if (s != deadZone) {
				//s.GetComponent<Renderer> ().enabled = true;

				toggle (s, true);
			}
		}

	}

	void toggle(Collider col,bool visible){
		foreach (var v in _content[col]) {
			v.enabled = visible;
		}
	}



}
