﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LilishipController : MonoBehaviour {


	//public enum LilishipState{Idle,On,Broken}

	//public LilishipState lilyState = LilishipState.Idle;
	public float lilyState = 0;//LilishipState.Idle;

	Animator animator;

	void Awake(){
		animator = GetComponent<Animator> ();
	}


	void Update () {
		animator.SetInteger ("Health", (int)Mathf.Floor(lilyState));
	}
}
