﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;


/*

check animations

top rope fixes

try crazy pivot motion


*/


public class BallonController : MonoBehaviour {


	public enum BallonLoopState{
		IdleLoop,PastoralLoop,StormyLoop,TornLoop,DodgeLoop,CrashLoop,SpinLoop
	}

	public enum BallonMotionState{
		Idle,Hit,DodgeLeft,DodgeRight,Crash
	}


	/*
	public float force = 1f;
	public float speed = 1f;
	public ForceMode mode = ForceMode.Force;
	*/

	public bool debugMode = false;
	public Rigidbody top;
	public Rigidbody bottom;
	public List<Cloth> cloths;
	public float clothRandomAcc = 1f;
	public Vector3 clothExternalAcc = Vector3.zero;

	//[Range(0,1f)]
	//public float animatorSpeed = 1f;

	[Range(0,1f)]
	public float loopWeight = 1f;


	public BallonLoopState loopState;
	public BallonMotionState motionState;

	Animator _animator;
	TransformData _topPos;
	TransformData _bottomPos;

	[SerializeField][HideInInspector]
	bool _needsRestore;



	// Use this for initialization
	void Start () {
		_animator = GetComponent<Animator> ();

		_topPos = new TransformData (){ position = top.transform.position, rotation = top.transform.rotation };
		_bottomPos = new TransformData (){ position = bottom.transform.position, rotation = bottom.transform.rotation };



	}
	
	// Update is called once per frame
	void Update () {

		var noise = new Vector3(clothRandomAcc,clothRandomAcc,clothRandomAcc);
		foreach (var cloth in cloths) {
			cloth.externalAcceleration = clothExternalAcc;
			cloth.randomAcceleration = noise;
		}

		_animator.SetLayerWeight (1, loopWeight);


		if (debugMode) {
			//var x = -CrossPlatformInputManager.GetAxis ("Horizontal");
			//core.AddForce (Vector3.forward * x * force * Time.deltaTime, mode);

			var stateName = (string)loopState.ToString ();
			if (stateName != "") {
				if (!_animator.GetCurrentAnimatorStateInfo (1).IsName (stateName)) {
					_animator.Play (stateName, 1,0);
				}
			}


			stateName = (string)motionState.ToString ();
			
			if (stateName != "") {
			  if (!_animator.GetCurrentAnimatorStateInfo (0).IsName (stateName)) {
					_animator.CrossFadeInFixedTime (stateName, 0.2f, 0);
						_animator.Play (stateName, 0,0);
				}
			}
		}
	

	}



	void Invalidate(){
		_needsRestore = true;
	}


	void Restore(){

		Debug.Log ("NeedsRestore:"+_needsRestore);

		if (_needsRestore) {
			top.transform.position = _topPos.position;
			top.transform.rotation = _topPos.rotation;

			bottom.transform.position = _bottomPos.position;
			bottom.transform.rotation = _bottomPos.rotation;
			_needsRestore = false;
		}

	}


	void SetBallonMotionState(BallonMotionState state){

		switch (state) {

		case BallonMotionState.Idle:

			top.transform.position = _topPos.position;
			top.transform.rotation = _topPos.rotation;

			bottom.transform.position = _bottomPos.position;
			bottom.transform.rotation = _bottomPos.rotation;

			break;

		default:break;

		}



	}

	/*
	void SetBallonLoopState(BallonLoopState state){

		//set constraints
		//toggle goals
		//position goals / animate goals
		//set goals speed

		//set cloth shape (random motion, external forces and weights
		//set loop speed
		//set loop motion


		//top.constraints = RigidbodyConstraints.

		//Cloth c = null;
	
		var bottomIdle = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY;
		var topIdle = RigidbodyConstraints.FreezePositionX;
	
		switch (state) {

		case BallonLoopState.PastoralLoop:
		case BallonLoopState.StormyLoop:
		case BallonLoopState.TornLoop:

		//	bottom.constraints = bottomIdle;
		//	top.constraints = topIdle;
			break;

		case BallonLoopState.Hit:
			break;
		case BallonLoopState.DodgeLoop:
			break;
		case BallonLoopState.SpinLoop:
			break;

		//default:break;
			
		}



	}

*/

}
