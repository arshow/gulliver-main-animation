﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.EventSystems;
/*

position them
carry
run


*/


public class LiliputsController : MonoBehaviour {


	public enum State{
		Tie,ThrowRopes,Rotate,Carry,Idle,Run
	}


	public Animator leader;
	public Transform ramkol;
	public Transform horizon;

	public float transTime = 0.2f;

	Animator[] _lilys;

	// Use this for initialization
	void Start () {

	
		_lilys = GetComponentsInChildren<Animator> ().Where(a=>a.gameObject != this.gameObject).ToArray();


	}




	public void Idle(){

		foreach (var l in _lilys) {

			l.transform.LookAt (ramkol, l.transform.up);

			l.CrossFade ("idle",transTime,0);
		}
	}


	public void Carry(){
		

		foreach (var l in _lilys) {
			//l.transform.LookAt (horizon, l.transform.up);
			l.transform.LookAt (ramkol, l.transform.up);

			l.CrossFade ("carry",transTime,0);
		}


	}


	public void King(){




		foreach (var l in _lilys) {
			l.transform.LookAt (horizon, l.transform.up);

			l.CrossFade ("idle",transTime,0);
		}

	}



	public void Run(){
		
		foreach (var l in _lilys) {
			//l.transform.LookAt (horizon, l.transform.up);

			//l.CrossFade ("idle",transTime,0);
		}
	}







}



/*
void Start()
    {
        EventTrigger trigger = GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback.AddListener((data) => { OnPointerDownDelegate((PointerEventData)data); });
        trigger.triggers.Add(entry);
    }

    public void OnPointerDownDelegate(PointerEventData data)
    {
        Debug.Log("OnPointerDownDelegate called.");
    }
*/