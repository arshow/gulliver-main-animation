﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiliputianController : MonoBehaviour {

	public Transform lookAt;
	public float turnDamping = 0.5f;
	public float crossFade = 0.2f;

	Animator _animator;

	void Awake() {
		_animator = GetComponent<Animator> ();
	}


	void Update(){


		if (lookAt != null) {

			var lookPos = lookAt.position - transform.position;
			//lookPos.x = 0;
			var rotation = Quaternion.LookRotation(lookPos);
			transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * turnDamping);

		}

	}


	void Idle(){
		_animator.CrossFade ("idle", crossFade, 0);
	}
	void Carry(){
		_animator.CrossFade ("carry", crossFade, 0);
	}

	void Rotate(){
		_animator.CrossFade ("rotate", crossFade, 0);
	}

	void Throw(){
		_animator.CrossFade ("throw", crossFade, 0);
	}
	void Catch(){
		_animator.CrossFade ("catch", crossFade, 0);
	}
	void Run(){
		_animator.CrossFade ("run", crossFade, 0);
	}

}
