﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chain : MonoBehaviour {


	public Transform[] segments;

	// Use this for initialization
	void Start () {

		for (int i = segments.Length -1; i > 0 ; i--) {

			var child = segments [i];
			var parent = segments [i - 1];

			var body = child.GetComponent<Rigidbody> ();
			if (body == null)
				body = child.gameObject.AddComponent<Rigidbody> ();

			var pbody = parent.GetComponent<Rigidbody> ();
			if (pbody == null)
				pbody = parent.gameObject.AddComponent<Rigidbody> ();

			var joint = body.gameObject.AddComponent<ConfigurableJoint> ();

			//joint.connectedAnchor

			joint.connectedBody = pbody;

			joint.xMotion = ConfigurableJointMotion.Locked;
			joint.yMotion = ConfigurableJointMotion.Locked;
			joint.zMotion = ConfigurableJointMotion.Locked;

			joint.angularXMotion = ConfigurableJointMotion.Free;
			joint.angularYMotion = ConfigurableJointMotion.Locked;
			joint.angularZMotion = ConfigurableJointMotion.Free;

		}


	}


	void OnDrawGizmos(){

		foreach (var s in segments) {

		//	var pos = s.

		}

	}
	

}
