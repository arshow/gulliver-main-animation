using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class ToggleBehaviour : PlayableBehaviour
{
	public double start;
	public double end;
}
