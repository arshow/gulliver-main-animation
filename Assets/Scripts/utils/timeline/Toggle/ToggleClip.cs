using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class ToggleClip : PlayableAsset, ITimelineClipAsset
{
    public ToggleBehaviour template = new ToggleBehaviour ();

    public ClipCaps clipCaps
    {
        get { return ClipCaps.Blending; }
    }

    public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<ToggleBehaviour>.Create (graph, template);
        return playable;
    }
}
