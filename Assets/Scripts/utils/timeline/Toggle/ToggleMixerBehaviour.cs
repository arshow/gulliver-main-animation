using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class ToggleMixerBehaviour : PlayableBehaviour
{

	public double start;
	public double end;

    Transform m_TrackBinding;
    bool m_FirstFrameHappened;
	bool m_default_state;

    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        m_TrackBinding = playerData as Transform;

        if (m_TrackBinding == null)
            return;

        if (!m_FirstFrameHappened)
        {
            m_FirstFrameHappened = true;
			m_default_state = m_TrackBinding.gameObject.activeSelf;
        }

        int inputCount = playable.GetInputCount ();
        for (int i = 0; i < inputCount; i++){
			float inputWeight = playable.GetInputWeight(i);
			m_TrackBinding.gameObject.SetActive (inputWeight > 0);
        }

    }




    public override void OnGraphStop (Playable playable)
    {
        m_FirstFrameHappened = false;

		if (m_TrackBinding == null)
			return;

		//if (!Application.isPlaying)
		//	return;

		int inputCount = playable.GetInputCount ();
		for (int i = 0; i < inputCount; i++){
			var inputHandle = playable.GetInput(i);
			if (!inputHandle.IsValid ()) {
			//	Debug.Log ("NOT VALID:"+m_TrackBinding.gameObject.name);

				if (Application.isPlaying) {
					m_TrackBinding.gameObject.SetActive (false);
				} else {
					m_TrackBinding.gameObject.SetActive (m_default_state);
				}

				continue;
			}

			float inputWeight = playable.GetInputWeight(i);
			m_TrackBinding.gameObject.SetActive (inputWeight > 0);
		}
    }





}

