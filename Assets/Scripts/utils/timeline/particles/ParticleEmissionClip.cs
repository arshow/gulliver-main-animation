﻿using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[System.Serializable]
public class ParticleEmissionClip:PlayableAsset,ITimelineClipAsset
{

	public ExposedReference<ParticleSystem> ps;
	ParticleEmissionPlayable template = new ParticleEmissionPlayable ();


	public ClipCaps clipCaps{
		get{ return ClipCaps.None;}
	}


	public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
	{
		var playable = ScriptPlayable<ParticleEmissionPlayable>.Create (graph, template);
		ParticleEmissionPlayable clone = playable.GetBehaviour ();
		clone.ps = ps.Resolve (graph.GetResolver());
		return playable;
	}

}


