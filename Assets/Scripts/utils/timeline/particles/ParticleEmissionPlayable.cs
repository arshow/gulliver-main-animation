﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[System.Serializable]
public class ParticleEmissionPlayable :PlayableBehaviour{


	public ParticleSystem ps;

	/*
	public override void OnGraphStart (Playable playable)
	{
		base.OnGraphStart (playable);
	}

	public override void OnGraphStop (Playable playable)
	{
		base.OnGraphStop (playable);
	}
	*/

	public override void OnBehaviourPlay (Playable playable, FrameData info)
	{
		base.OnBehaviourPlay (playable, info);
		var emission = ps.emission;
		emission.enabled = true;
	}


	public override void OnBehaviourPause (Playable playable, FrameData info)
	{
		base.OnBehaviourPause (playable, info);
		var emission = ps.emission;
		emission.enabled = false;
	}
}
