﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using System.Linq;


[System.Serializable]
public class ReparentBehaviour :PlayableBehaviour{


	public Transform parent;
	Transform _target;
	Transform _defaultParent;

	public void Initialize(Transform parent){
		this.parent = parent;
	}

	public override void ProcessFrame (Playable playable, FrameData info, object playerData)
	{
		base.ProcessFrame (playable, info, playerData);

		if (_target == null) {
			_target = (Transform)playerData;
		}

		if (_defaultParent == null) {
			_defaultParent = _target.parent;
		}

		if (_defaultParent == parent) {
			
			Debug.LogError ("default parent and parent are the same???");

		} else {

			if(parent != null && _target.parent != parent){
			//	_target.parent = parent;
			}
		}

	
	}


	public override void OnBehaviourPause (Playable playable, FrameData info)
	{
		base.OnBehaviourPause (playable, info);
		if (_target != null) {
			_target.parent = _defaultParent;
		}



		var localTime = playable.GetTime ();
		var futureTime = localTime + Time.deltaTime;
		var duration = playable.GetDuration ();
		var worldTime = playable.GetGraph ().GetRootPlayable (0).GetTime ();


		Debug.Log ("Reparent:Behaviour_Pause:" + localTime +" "+futureTime+" " + worldTime  +" "+ playable.GetDuration() );


		//if (playable.GetTime () + info.deltaTime * 2f >= playable.GetDuration ()) {


		//if(currentTime > info

	//	playable.GetTime()

		/*
		if (localTime <= 0) {
			Debug.Log ("Turn Me Off!!!! pre play");
		}else if (localTime >= playable.GetDuration()) {
			Debug.Log ("Turn Me Off!!!! equal or over");
		}else if (Mathf.Approximately((float)localTime,(float)duration)) {
			Debug.Log ("Turn Me Off!!!!  approx");
		}else if (futureTime >= duration ) {
			Debug.Log ("Turn Me Off!!!!  delta");
		}else if (Mathf.Approximately((float)futureTime,(float)duration)) {
			Debug.Log ("Turn Me Off!!!!  delta approx");
		}

		*/
	}


	public override void OnBehaviourPlay (Playable playable, FrameData info)
	{
		base.OnBehaviourPlay (playable, info);
		//Debug.Log ("Reparent:Behaviour_Play");


	}



	public override void OnGraphStop (Playable playable)
	{
		base.OnGraphStop (playable);
		//Debug.Log ("Reparent:Graph_Stop");
	}


	public override void OnPlayableDestroy (Playable playable)
	{
		base.OnPlayableDestroy (playable);
		//Debug.Log ("Reparent:OnPlayableDestroy");
	}

}
