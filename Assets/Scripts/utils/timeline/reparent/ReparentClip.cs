﻿using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using System.Linq;

[System.Serializable]
public class ReparentClip:PlayableAsset,ITimelineClipAsset
{

	public ExposedReference<Transform> parent;

	ReparentBehaviour template = new ReparentBehaviour ();


	public ClipCaps clipCaps{
		get{ return ClipCaps.All;}
	}

		
	public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
	{
		var playable = ScriptPlayable<ReparentBehaviour>.Create (graph, template);
		ReparentBehaviour clone = playable.GetBehaviour ();
		clone.Initialize (parent.Resolve(graph.GetResolver()));
		return playable;
	}



}


