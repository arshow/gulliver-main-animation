﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using System.Collections.Generic;


[TrackColor(0,0.561f, 0.114f)]
[TrackClipType(typeof(ReparentClip))]
[TrackBindingType(typeof(Transform))]
public class ReparentTrack : TrackAsset
{

	public override void GatherProperties (PlayableDirector director, IPropertyCollector driver)
	{
		#if UNITY_EDITOR
		Transform trackBinding = director.GetGenericBinding(this) as Transform;
		if (trackBinding == null)
			return;

		var serializedObject = new UnityEditor.SerializedObject (trackBinding);
		var iterator = serializedObject.GetIterator();
		while (iterator.NextVisible(true))
		{
			if (iterator.hasVisibleChildren)
				continue;

			driver.AddFromName<Transform>(trackBinding.gameObject, iterator.propertyPath);
		}
		#endif
		base.GatherProperties (director, driver);
	}
}
