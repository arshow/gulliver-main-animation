﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using System.Collections.Generic;


[TrackColor(0.933f,0.561f, 0.114f)]
[TrackClipType(typeof(StateClip))]
[TrackBindingType(typeof(Animator))]
public class StateTrack : TrackAsset
{

	public int layer;



	/*
	public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
	{
		return ScriptPlayable<ParticleCountMixerBehaviour>.Create (graph, inputCount);
	}
	*/

	public override void GatherProperties (PlayableDirector director, IPropertyCollector driver)
	{
		#if UNITY_EDITOR
		Animator trackBinding = director.GetGenericBinding(this) as Animator;
		if (trackBinding == null)
			return;

		var serializedObject = new UnityEditor.SerializedObject (trackBinding);
		var iterator = serializedObject.GetIterator();
		while (iterator.NextVisible(true))
		{
			if (iterator.hasVisibleChildren)
				continue;

			driver.AddFromName<Animator>(trackBinding.gameObject, iterator.propertyPath);
		}
		#endif
		base.GatherProperties (director, driver);
	}
}
