﻿using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using System.Linq;

[System.Serializable]
public class StateClip:PlayableAsset,ITimelineClipAsset
{

	public string state;
	public int layer = 0;

	StatePlayable template = new StatePlayable ();


	public ClipCaps clipCaps{
		get{ return ClipCaps.All;}
	}

		
	public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
	{
		var playable = ScriptPlayable<StatePlayable>.Create (graph, template);
		StatePlayable clone = playable.GetBehaviour ();
		clone.Initialize (state, layer);

		//Debug.Log ("BLA:"+this.name);

		if (state != "") {
		//	this.name = state;
			//Debug.Log ("setName:"+state);
		}



		return playable;
	}



}


