﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using System.Linq;


[System.Serializable]
public class StatePlayable :PlayableBehaviour{


	public string state;
	public int layer;

	Animator _animator;
	bool _isPlaying = false;
	float _stateDuration = 0f;
	bool _dirtyState;

	public void Initialize(string state,int layer){
		this.state = state;
		this.layer = layer;
		_dirtyState = true;
	}


	public override void ProcessFrame (Playable playable, FrameData info, object playerData)
	{
		base.ProcessFrame (playable, info, playerData);

		if (_animator == null) {
			_animator = (Animator)playerData;
		}
			
		#if UNITY_EDITOR
		if(_dirtyState){
			var hasState = HasState(_animator,state,layer);
			if(!hasState){
				throw new UnityException("StatePlayable Error: state:" + state + " layer:" + layer + " doesnt exist in " + _animator );
			}else{
				_dirtyState = false;
			}
		}
		#endif

		if (Application.isPlaying) {

			if (_isPlaying == false && _animator != null) {
				_animator.Play (state, layer);
				//_animator.CrossFade (state, 0.2f, layer);
				_isPlaying = true;
			}

		} else {

			#if UNITY_EDITOR
			updateAnimatorTime (playable);
			#endif

		}

	}


	public override void OnBehaviourPlay (Playable playable, FrameData info)
	{
		base.OnBehaviourPlay (playable, info);
		_isPlaying = false;

	}





	#if UNITY_EDITOR
	void updateAnimatorTime(Playable playable){

		var clipTime = playable.GetTime ();

		var stateTime = GetStateDuration (_animator, state);

		var time = (float)(clipTime/stateTime);

		_animator.Play (state, layer,time);

		_animator.Update(0);

	//	Debug.Log ("Update?: "+state + " clip:" + clipTime + " state:" + stateTime + " time:"+time );
	}



	private static UnityEditor.Animations.AnimatorState[] GetStateNames(Animator animator) {
		var controller = animator ? animator.runtimeAnimatorController as UnityEditor.Animations.AnimatorController : null;
		return controller == null ? null : controller.layers.SelectMany(l => l.stateMachine.states).Select(s => s.state).ToArray();
	}

	private static float GetStateDuration(Animator animator,string name) {
		var states = GetStateNames (animator);
		var s = states.FirstOrDefault (st=>st.name == name);
		return s == null ? 0 : s.motion.averageDuration / s.speed;
	}

	private static bool HasState(Animator animator,string name,int layer){
		var controller = animator.runtimeAnimatorController as UnityEditor.Animations.AnimatorController;
		return controller.layers [layer].stateMachine.states.FirstOrDefault (s => s.state.name == name).state != null;
	}

	#endif


}
