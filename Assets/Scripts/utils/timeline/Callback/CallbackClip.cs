using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class ClipEvent:UnityEngine.Events.UnityEvent<string>{}


[Serializable]
public class CallbackClip : PlayableAsset, ITimelineClipAsset
{

	public ClipEvent callback;
    public CallbackBehaviour template = new CallbackBehaviour ();

    public ClipCaps clipCaps
    {
        get { return ClipCaps.Blending; }
    }

    public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<CallbackBehaviour>.Create (graph, template);
		playable.GetBehaviour ().callback = callback;//.Resolve (graph.GetResolver());
        return playable;
    }
}
