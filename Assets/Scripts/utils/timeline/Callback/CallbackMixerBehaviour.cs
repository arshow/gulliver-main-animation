using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class CallbackMixerBehaviour : PlayableBehaviour
{

    Transform m_TrackBinding;
    bool m_FirstFrameHappened;

    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        m_TrackBinding = playerData as Transform;

        if (m_TrackBinding == null)
            return;

        if (!m_FirstFrameHappened)
        {
            m_FirstFrameHappened = true;
        }

        int inputCount = playable.GetInputCount ();

        float totalWeight = 0f;
        float greatestWeight = 0f;
        int currentInputs = 0;

        for (int i = 0; i < inputCount; i++)
        {
            float inputWeight = playable.GetInputWeight(i);
            ScriptPlayable<CallbackBehaviour> inputPlayable = (ScriptPlayable<CallbackBehaviour>)playable.GetInput(i);
            CallbackBehaviour input = inputPlayable.GetBehaviour ();
            
            totalWeight += inputWeight;



            if (!Mathf.Approximately (inputWeight, 0f))
                currentInputs++;
        }

    }

    public override void OnGraphStop (Playable playable)
    {
        m_FirstFrameHappened = false;

        if (m_TrackBinding == null)
            return;

    }

	public override void OnBehaviourPlay (Playable playable, FrameData info)
	{
		base.OnBehaviourPlay (playable, info);

		int inputCount = playable.GetInputCount ();

		for (int i = 0; i < inputCount; i++)
		{
			float inputWeight = playable.GetInputWeight(i);
			ScriptPlayable<CallbackBehaviour> inputPlayable = (ScriptPlayable<CallbackBehaviour>)playable.GetInput(i);
			CallbackBehaviour input = inputPlayable.GetBehaviour ();
			input.callback.Invoke ("hjhjhjhjhjhjh");


		}
	}
}
