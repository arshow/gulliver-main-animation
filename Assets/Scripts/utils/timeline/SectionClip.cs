﻿using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[System.Serializable]
public enum SectionWrapMode
{
	Stop,Loop,None
}



[System.Serializable]
public class SectionClip:PlayableAsset,ITimelineClipAsset
{


	public SectionWrapMode wrapMode = SectionWrapMode.Stop;


	SectionPlayable template = new SectionPlayable ();


	public ClipCaps clipCaps{
		get{ return ClipCaps.None;}
	}


	public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
	{
		var playable = ScriptPlayable<SectionPlayable>.Create (graph, template);
		SectionPlayable clone = playable.GetBehaviour ();
		clone.timeline =  owner.GetComponent<PlayableDirector> ();
		clone.wrapMode = wrapMode;
		return playable;
	}

}


