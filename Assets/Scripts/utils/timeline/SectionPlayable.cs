﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[System.Serializable]
public class SectionPlayable :PlayableBehaviour{




	public PlayableDirector timeline;
	public SectionWrapMode wrapMode = SectionWrapMode.Stop;
	//bool _pendingPause;
	double _stamp;


	public override void OnGraphStop (Playable playable)
	{
		base.OnGraphStop (playable);
	}


	public override void OnBehaviourPlay (Playable playable, FrameData info)
	{
		base.OnBehaviourPlay (playable, info);
		if(timeline != null){
			_stamp = timeline.time;
		}
	}


	/*
	public override void ProcessFrame (Playable playable, FrameData info, object playerData)
	{
		base.ProcessFrame (playable, info, playerData);


	}
*/


	public override void PrepareFrame (Playable playable, FrameData info)
	{
		base.PrepareFrame (playable, info);

		if (playable.GetTime () + info.deltaTime * 2f >= playable.GetDuration ()) {
			//Debug.Log ("Done!!");

			if (Application.isPlaying && timeline != null) {
				timeline.Pause ();
			}


		}


	}





	public override void OnBehaviourPause (Playable playable, FrameData info)
	{
		base.OnBehaviourPause (playable, info);

		return;


		Debug.Log ("Pause!"+playable.GetTime () == playable.GetDuration () + " " + playable.IsDone());

		if (Application.isPlaying && timeline != null && timeline.time > 0) {
		//	timeline.Pause ();
		}


		

		return;





		if(Application.isPlaying && timeline != null && timeline.time > 0){

			if (wrapMode == SectionWrapMode.Stop) {
				Time.timeScale = 0;
			} else if (wrapMode == SectionWrapMode.Loop) {
				if (_stamp > 0) {
					timeline.time = _stamp;
				}
			} else {
				//Debug.Log ("Noting!");
			}

		}



	}
}
