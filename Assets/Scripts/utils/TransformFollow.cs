﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformFollow : MonoBehaviour {

	public Transform target;
	public bool followPosition = true;
	public bool followRotation = false;

	
	// Update is called once per frame
	void LateUpdate () {
		if (target != null) {
			if (followRotation) {
				this.transform.rotation = target.rotation;
			}

			if (followPosition) {

				var pos = target.position;


				this.transform.position = target.position;
			}
		}
	}

}
