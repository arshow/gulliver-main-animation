﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTransform : MonoBehaviour {

	//public Transform target;
	public Vector3 rotationVelocity = Vector3.zero;

	public bool rotateX = true;
	public bool rotateY = true;
	public bool rotateZ = true;
	
	// Update is called once per frame
	void Update () {

		if(!rotateX && !rotateY && !rotateZ){
			return;
		}



		var euler = transform.rotation.eulerAngles;
		if (rotateX) {
			euler.x += rotationVelocity.x * Time.deltaTime;
		}
		if (rotateY) {
			euler.y += rotationVelocity.y * Time.deltaTime;
		}
		if (rotateZ) {
			euler.z += rotationVelocity.z * Time.deltaTime;
		}

		transform.rotation = Quaternion.Euler (euler);

		
	}


}
