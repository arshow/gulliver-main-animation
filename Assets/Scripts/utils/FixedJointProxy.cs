﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class FixedJointProxy : MonoBehaviour {

	public Transform parent;
	Transform _proxy;
	Rigidbody _body;

	// Use this for initialization
	void Start () {
		
		_body = GetComponent<Rigidbody> ();

		_proxy = new GameObject (transform.name + "_proxy").transform;
		_proxy.position = transform.position;
		_proxy.rotation = transform.rotation;
		_proxy.SetParent (parent, true);

		/*
		var jointBody = _proxy.gameObject.AddComponent<Rigidbody> ();
		jointBody.mass = 0.01f;
		jointBody.isKinematic = true;
		var joint = gameObject.AddComponent<FixedJoint> ();
		joint.connectedBody = jointBody;
		*/

		_body.isKinematic = true;

	}


	void LateUpdate(){
		_body.MovePosition (_proxy.position);
		_body.MoveRotation (_proxy.rotation);
		//_body.transform.position = _proxy.position;
		//_body.transform.rotation = _proxy.rotation;
	}

	

}
