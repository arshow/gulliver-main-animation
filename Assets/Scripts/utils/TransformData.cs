﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class TransformData 
{

	public Vector3 position;
	public Quaternion rotation;

}

