﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


[RequireComponent(typeof(Rigidbody))]
public class RigidBodyProxy : MonoBehaviour {


	public bool FreezeX;
	public bool FreezeY;
	public bool FreezeZ;
	public bool FreezeRotationX;
	public bool FreezeRotationY;
	public bool FreezeRotationZ;

	Rigidbody _body;

	void Awake(){
		_body = GetComponent<Rigidbody> ();
	}


	void Update(){

		var constraints = _body.constraints;

		constraints = Toggle (constraints, RigidbodyConstraints.FreezePositionX, FreezeX);
		constraints = Toggle (constraints, RigidbodyConstraints.FreezePositionY, FreezeY);
		constraints = Toggle (constraints, RigidbodyConstraints.FreezePositionZ, FreezeZ);
		constraints = Toggle (constraints, RigidbodyConstraints.FreezeRotationX, FreezeRotationX);
		constraints = Toggle (constraints, RigidbodyConstraints.FreezeRotationY, FreezeRotationY);
		constraints = Toggle (constraints, RigidbodyConstraints.FreezeRotationZ, FreezeRotationZ);

		if (constraints != _body.constraints) {
			_body.constraints = constraints;
		}


	}


	RigidbodyConstraints Toggle(RigidbodyConstraints constraints,RigidbodyConstraints value,bool state){
		if (state) {
			constraints |= value;
		} else {
			constraints &= ~value;
		}
		return constraints;
	}
		




}
