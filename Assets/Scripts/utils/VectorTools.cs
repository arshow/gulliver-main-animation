﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vectors  {



	public static Vector2 Mul(this Vector2 v1, Vector2 v2) 
	{
		return Vector2.Scale (v1, v2);
	}


	public static Vector3 Mul(this Vector3 v1, Vector3 v2) 
	{
		return Vector3.Scale (v1, v2);
	}


	//0,0,1    1,1,0
	//1,0,1    0,1,0
	public static Vector3 Flip(this Vector3 v1) 
	{
		return Vector3.one - v1;
	}


	public static float Sum(this Vector3 v1) 
	{
		return v1.x + v1.y + v1.z;
	}

}
