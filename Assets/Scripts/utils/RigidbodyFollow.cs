﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RigidbodyFollow : MonoBehaviour {

	public Rigidbody target;
	public float damping = 1f;
	public Vector3 axis = Vector3.one;



	// Use this for initialization
	void Awake () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (!target)
			return;
	

		//take source value 2,6,5
		//multiply by mask   0,0,1 * 2,6,5 = 0,0,5

		//take current value 3,2,3
		//multiply by invert mask   1,1,0 * 3,2,3 = 3,2,0

		//add to current   3,2,0 + 0,0,5 = 3,2,5
	
		var goalPos = transform.position.Mul(axis);
		var bodyPos = target.position.Mul(Vectors.Flip(axis));
		var pos = goalPos + bodyPos;

		target.MovePosition(Vector3.Lerp(target.position,pos,Time.deltaTime * damping));


	}




}
