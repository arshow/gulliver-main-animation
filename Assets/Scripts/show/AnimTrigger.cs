﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class AnimTrigger :MonoBehaviour {

	public enum AnimState{
		Init,Playing,Exiting
	}

	public float playDelay = 10;
	//public AudioClip soundClip;
	public float soundDelay = 0;

	public AnimState state;

	public virtual void Restart(){
		state = AnimState.Init;
	}

	public virtual void PlayEnd(){
		state = AnimState.Init;
	}

	public virtual void Play(){
		state = AnimState.Playing;



		var audio = GetComponent<AudioSource>();
		if (audio != null && audio.enabled && !audio.isPlaying) {
			if(soundDelay > 0){
				audio.Play ();
			}else {
				audio.PlayDelayed(soundDelay);
			}
		}

	}

	public virtual void Exit(){
		state = AnimState.Exiting;
		Restart ();
	}

}
