﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using System.Linq;


public class GulliverEvent:UnityEngine.Events.UnityEvent{




}



public class TimelineController : MonoBehaviour {

	public SceneController ctrl;
	public PlayableDirector[] timelines;


	public PlayableDirector activeTimeline;

	// Use this for initialization
	void Start () {


		var time = 0d;

		foreach (var director in timelines) {

			var main = director.playableAsset.outputs.First (o => o.streamName == "Main");

			PlayableTrack at = (PlayableTrack)main.sourceObject;


			/*
			if (ctrl.autoPlay) {

				at.muted = true;
				continue;

			}
			*/


			var triggers = new List<TimelineTrigger> ();
			var clips = at.GetClips ().ToArray();


			for (int i = 0; i < clips.Length; i++) {

				var clip = clips [i];

				var go = new GameObject (clip.displayName);
				go.transform.parent = ctrl.transform;

				var trigger = go.AddComponent<TimelineTrigger> ();
				trigger.controller = this;
				trigger.clip = clip;
				trigger.director = director;
				trigger.name = clip.displayName;

				trigger.playDelay = (float)time;

				time += clip.duration;

				ctrl.animations.Add (trigger);

			}



			DisableTimeline (director);



		}



	}



	public void DisableTimeline(PlayableDirector director){
		foreach (var output in director.playableAsset.outputs) {
			if (output.sourceObject is ToggleTrack && !((ToggleTrack)output.sourceObject).muted) {
				var obj = (Transform)director.GetGenericBinding(output.sourceObject);
				if (obj != null) {
					obj.gameObject.SetActive (false);
				}
			}
		}
	}







	public void Export(){

		//var go = new GameObject ("interp");

		var interp = GameObject.Find("Stage").GetComponent<SceneInterpreter> ();

	
		//interp.Actions = 


		for (int i = 0; i <  ctrl.animations.Count; i++) {

			var trigger = ctrl.animations [i];

			var callback = new ARShowAction ();

			callback.Name = trigger.name;
			callback.Layer = "Root";


			var play = new UnityEngine.Events.UnityEvent ();


			play.AddListener (() => {
				//sctrl.PlayAnimationByIndex(i);
			});


		
			callback.OnPlay = play;
			/*
			callback.OnStop.AddListener (() => {
				ctrl.StopAnimationByIndex(i);
			});
			*/


			interp.Actions.Add (callback);


				
		}

	}



}
