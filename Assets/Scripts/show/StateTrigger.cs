﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateTrigger : AnimTrigger {

	public Animator target;
	public string currentState;
	public string prevState;

	void Awake () {

	}

	public override void Play ()
	{
		base.Play ();
		target.Play (currentState,0,0);
	}


	public override void Restart ()
	{
		base.Restart ();
		target.Play (prevState,0,1f);

	}


}




