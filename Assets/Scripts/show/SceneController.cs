﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class SceneController : MonoBehaviour {

    
	public List<AnimTrigger> animations;
	public bool autoPlay = false;
	public Transform worldGrid;

	public AnimTrigger activeClip;

	[HideInInspector]
	public bool isPlaying;

	Animator _animator;
	AudioSource _audio;
	Coroutine _player;


	public AudioSource audioPlayer{
		get{ return _audio; }
	}

	void Awake(){


		#if UNITY_ANDROID
		Application.targetFrameRate = 30;
		#endif

		_animator = GetComponent<Animator> ();
		_audio = GetComponent<AudioSource> ();
		animations = GetComponentsInChildren<AnimTrigger> ().ToList();
		/*
		var stateAnims = GetComponentsInChildren<StateTrigger> ();
		stateAnims [0].prevState = "Idle";
		for (int i = 1; i < stateAnims.Length; i++) {
			stateAnims [i].prevState = stateAnims [i - 1].currentState;
		}
		*/
	}



	void Start(){
		

		foreach (var anim in animations) {
			//anim.enabled = true;

			var named = animations.Where (a => a.name == anim.name);
			if (named.Count () > 1) {
				throw new UnityException ("multiple names:" + anim.name +  " " + named.Count());
			}

		}

		if (autoPlay) {
			PlayAll ();
		}
	}


	public void Next(){

		AnimTrigger current = null;

		if (activeClip != null) {

			var index = animations.IndexOf (activeClip);
			index++;
			index %= animations.Count;
			current = animations [index];


		} else {
			current = animations [0];
		}


		current.Play ();

	}



	public void Restart(){

		if (_player != null) {
			StopCoroutine (_player);
		}

		isPlaying = false;


		for (int i = animations.Count - 1; i > -1 ;i--) {
			animations [i].Restart ();
		}
	}



	public void PlayAll(){
		if (!isPlaying) {
			_player = StartCoroutine (playAll ());
		}
	}



	IEnumerator playAll(){
		
		isPlaying = true;

		foreach (var anim in animations) {
			yield return new WaitForSeconds (anim.playDelay);
			anim.Play ();
		}

		isPlaying = false;

	}



	/*
    public void PlayAnimationByIndex(int index){
        animations[index].Play();
    }
	public void PlayAnimationEndByIndex(int index){
		animations [index].PlayEnd ();
	}
	public void StopAnimationByIndex(int index){
        animations[index].Restart();
	}
	*/

	public void PlayAnimationByIndex(string name){
		animations.Find (a => a.name == name).Play();
	}
	public void PlayAnimationEndByIndex(string name){
		animations.Find (a => a.name == name).PlayEnd();
	}
	public void StopAnimationByIndex(string name){
		animations.Find (a => a.name == name).Restart();
	}



	public void ShowGrid(){
		if (worldGrid != null) {
			worldGrid.gameObject.SetActive (true);
		}
	}

	public void HideGrid(){
		if (worldGrid != null) {
			worldGrid.gameObject.SetActive (false);
		}
	}

	public void ToggleGrid(){
		if (worldGrid != null) {
			worldGrid.gameObject.SetActive (!worldGrid.gameObject.activeSelf);
		}
	}



	/*
	1globe
	2globeAndClouds
	3globeToBallon
	4ballonFlight
	5BallonDodgeRight
	6BallonDodegLeft
	7City
	8Zepllin
*/



	/*
	void play<T>() where T:AnimTrigger{
		var anim = animations.First (a=>a.GetType() == typeof(T));
		//anim.Restart ();
		anim.Play ();
	}

	void stop<T>() where T : AnimTrigger
	{
		var anim = animations.First(a => a.GetType() == typeof(T));
		//anim.Restart ();
		anim.Exit();
	}

	void restrat<T>() where T : AnimTrigger
	{
		var anim = animations.First(a => a.GetType() == typeof(T));
		anim.Restart ();
		//anim.Exit();
	}

	*/
	/*
	void replay<T>() where T:AnimTrigger{
		var anim = animations.First (a=>a.GetType() == typeof(T));
		anim.Restart ();
		anim.Play ();
	}*/



}



#if UNITY_EDITOR


[CustomEditor(typeof(SceneController))]
public class SceneControllerEditor:Editor{


	SceneController ctrl;
	SerializedProperty anims;

	void OnEnable()
	{
		ctrl = (SceneController)target;
		anims = serializedObject.FindProperty("animations");
	}

	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		DrawDefaultInspector ();



		if (Application.isPlaying) {
			if (GUILayout.Button ("PlayAll")) {
				ctrl.PlayAll ();
			}
			if (GUILayout.Button ("Reset")) {
				ctrl.Restart ();
			}

			/*
			if (!ctrl.isPlaying) {
				if (GUILayout.Button ("PlayAll")) {
					ctrl.PlayAll ();
				}
			} else {
				if (GUILayout.Button ("Reset")) {
					ctrl.Restart ();
				}
			}
			*/
			EditorGUILayout.Space ();
		}




		var i = 0;
		foreach (var anim in ctrl.animations) {

			EditorGUILayout.BeginHorizontal ();

				
			EditorGUILayout.LabelField (anim.name);

			if (GUILayout.Button ("Play")) {
				anim.Play ();
			}

			if (GUILayout.Button ("Exit")) {
				anim.Exit ();
			}
			if (GUILayout.Button ("Reset")) {
				anim.Restart ();
			}


			EditorGUI.BeginChangeCheck ();
			var delay = EditorGUILayout.FloatField (anim.playDelay);
			if (EditorGUI.EndChangeCheck ()) {
				var el = serializedObject.FindProperty ("animations").GetArrayElementAtIndex (i);
				var obj = new SerializedObject (el.objectReferenceValue);
				obj.FindProperty ("playDelay").floatValue = delay;
				obj.ApplyModifiedProperties ();

			}


			EditorGUILayout.EndHorizontal ();

			i++;

		}

		serializedObject.ApplyModifiedProperties();


	}


}




#endif