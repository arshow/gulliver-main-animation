﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using System.Linq;

public class TimelineTrigger : AnimTrigger {

	[HideInInspector]
	public PlayableDirector director;

	[HideInInspector]
	public TimelineClip clip; 

	[HideInInspector]
	public TimelineController controller;


	public override void Play ()
	{
		base.Play ();


		updateDirector ();
		director.time = clip.start;

		if (director.state == PlayState.Paused) {
			director.Play ();
		}

	

	}


	public override void Restart ()
	{
		base.Restart ();
		updateDirector ();
		director.time = clip.start;
		director.Pause ();
	}

	public override void PlayEnd ()
	{
		base.PlayEnd ();
		updateDirector ();
		director.time = clip.end;
		director.Pause ();
	}



	void updateDirector(){


		if (!director.gameObject.activeSelf) {
			director.gameObject.SetActive (true);
			if (!director.enabled) {
				director.enabled = true;
			}
		}


		if (controller.activeTimeline != director) {
			if (controller.activeTimeline != null) {
				
				controller.activeTimeline.time = controller.activeTimeline.duration;
				controller.activeTimeline.Stop();

				controller.DisableTimeline (controller.activeTimeline);

			}

			controller.activeTimeline = director;
		}


		controller.ctrl.activeClip = this;
	

	}







}
