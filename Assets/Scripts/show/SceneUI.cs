﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneUI : MonoBehaviour {

	public bool enable;
	public Button toggle;
	public Button next;
	public ScrollRect scroll;
	public Transform content;
	public Text currentClip;
	bool _init;
	SceneController ctrl;
	public Text info;

	// Use this for initialization
	void Start () {

		 ctrl = FindObjectOfType<SceneController> ();

		transform.GetChild (0).gameObject.SetActive (enable);
		if (!enable) {
			return;
		}

		toggle.gameObject.SetActive (true);
		scroll.gameObject.SetActive (false);

		toggle.onClick.AddListener (onClick);
	


		next.onClick.AddListener (ctrl.Next);




		var log = "graphicsDeviceName:" + SystemInfo.graphicsDeviceName;
		log += " graphicsDeviceType"+SystemInfo.graphicsDeviceType;
		log += " graphicsDeviceVersion"+SystemInfo.graphicsDeviceVersion;
		log += " graphicsMultiThreaded"+SystemInfo.graphicsMultiThreaded;
		log += " graphicsShaderLevel"+SystemInfo.graphicsShaderLevel;
		if (info != null) {
			info.text = log;

		}


	}



	void Update(){

		if (ctrl.activeClip != null) {
			currentClip.text = ctrl.activeClip.name;
		}


	}

	void onClick(){

		if (!_init) {
			_init = true;
			fill ();
		}

		scroll.gameObject.SetActive (!scroll.gameObject.activeSelf);
	}


	void fill(){





		var prefab = content.GetComponentInChildren<Button> ();

		for (int i = 0; i < ctrl.animations.Count; i++) {
			
			Button btn = null;

			if (i > 0) {
				btn = (Button)Instantiate (prefab);
			} else {
				btn = prefab;
			}





			btn.name = ctrl.animations [i].name;
			btn.GetComponentInChildren<Text>().text = ctrl.animations [i].name;
			btn.interactable = true;

			btn.transform.SetParent(content,false);
			btn.onClick.AddListener( ()=>{

				var anim = ctrl.animations.Find(a=>a.name == btn.name);
				anim.Play();
				onClick();
			});



		}

	}


}
